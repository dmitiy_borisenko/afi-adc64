//
//    Copyright 2010 Alexey Shutov, Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef DOMINOSETTINGS_H
#define DOMINOSETTINGS_H

#include <QMap>
#include <QMetaType>
#include <QVariant>
#include <QVector>

#include "mldiscover/DeviceIndex.h"

class QSettings;
class RootConfig;

typedef union {
    quint16 all = {};
    struct {
        unsigned int timer_en: 1;
        unsigned int threshold_en: 1;
        unsigned int ttl_en: 1;
        unsigned int unused2: 13;
    } bit;
} reg_trig_ctrl;

typedef union {
    quint16 all = {};
    struct {
        unsigned int polarity1: 1;
        unsigned int shaper1_1: 1;
        unsigned int shaper1_2: 1;
        unsigned int shaper1_3: 1;
        unsigned int gain1_1: 1;
        unsigned int gain1_2: 1;
        unsigned int preamp_en1: 1;
        unsigned int unused1: 1;
        unsigned int polarity2: 1;
        unsigned int shaper2_1: 1;
        unsigned int shaper2_2: 1;
        unsigned int shaper2_3: 1;
        unsigned int gain2_1: 1;
        unsigned int gain2_2: 1;
        unsigned int preamp_en2: 1;
        unsigned int unused2: 1;
    } bit;
} reg_pca_struct;


struct ad5622operation {
    quint16 data_baseline = 0;
    quint16 data_level = 0;
/*
    unsigned int power_down_modes: 2;
    unsigned int reserved1: 2;
    unsigned int read_operation: 1;
    unsigned int hw_address: 2;
    unsigned int reserved2: 5;
    unsigned int unused: 8;
*/
};

struct SparseParams {
    bool en = false;
    bool sparseAllowed = true; // on false readout all readCellNumber (in adc64 app only)
    quint16 readCellNum = 4;
    quint16 offset = 0;
    quint16 period = 2;
    quint8 pointNumber = 0;
};

enum {
    AMP_BOARD_NONE       = 0x0,
    AMP_BOARD_PCA        = 0x1, // deprecated
    AMP_BOARD_AD5622     = 0x2,
    AMP_BOARD_AD5622v2   = 0x3
};

class FirParams
{
public:
    FirParams();
    int getRoundoffMax() const {return 3;}
    void setRoundoff(int value);
    QString presetKey;
    bool en = false;
    int roundoff = 1;
    QVector<qint16> coef;
};

class DspParams
{
public:
    bool getFirEnable() const { return en && fir.en; }
    bool getMafEnable() const { return en && maf_enable; }
    bool getTestEnable() const { return en && test_enable; }
    uint getMafType() const { uint v=0; if(maf_enable) v+=1; if(test_enable) v+=2; return v; }

    FirParams fir;
    bool en = false;
    int blc_thr = 100;
    bool maf_enable = false;
    int maf_tap_sel = 2;
    bool test_enable = false;
};

class DominoSettings
{
public:
    DominoSettings();
    void root_to_values(RootConfig root);
    RootConfig values_to_root();

    void copyFrom(const DominoSettings &ds);
    void setNCh(int n);
    void readFirPreset(const QString &firPreset, QSettings &s);
    void set_en_all(bool state);
    void setInvert(bool invert);
    QString getSettingsKey() const;
    static DeviceIndex getIndexBySettingsKey(const QString &key);
    static QStringList getFirPresets(QSettings &s);
    QString toString() const;
    int getHwVer() const;

    QMap<int, bool> chEn;
    QMap<int, int> chBaseline;  //device serial id - channel number - baseline
    QMap<int, bool> chTrigEn;
    QMap<int, int> chTrigThr;
    QMap<int, int> chZsThr;

    DeviceIndex devIndex;
    quint16 device_id = 0;
    quint32 device_serial = 0;
    QString serial;

    QString hwStr; // LLDP-MED[5] Hardware Revision
    bool enabled = false;

    int nch = 0;
    bool run = false;
    bool skipRestartRun = false;
    int readCellNumber = 1024;
    int trigDelay = 5;
    bool invertInput = false;
    bool zs_en = false;
    bool invert_thr_trig = false;
    bool invert_zs_thr = false;
    bool softwareZs = false;
    reg_trig_ctrl trigsetup = {};
    reg_pca_struct reg_pca = {};
    ad5622operation ad5622op;
    quint16 amp_board_set = 0;
    SparseParams sparseParams;
    DspParams dspParams;
    bool mstreamEnable = false;
    int mtuSize = 1500;
    int key = 0; // to distinguish evolution of setup
};
Q_DECLARE_METATYPE(DominoSettings)

#endif // DOMINOSETTINGS_H
