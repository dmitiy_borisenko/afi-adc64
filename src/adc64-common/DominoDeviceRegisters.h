//
//    Copyright 2014 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef DOMINODEVICEREGISTERS_H
#define DOMINODEVICEREGISTERS_H

#include "mregdev/mregdevice.h"

enum { MEM_BIT_SELECT_CTRL = 1<<13 };

// Control registers
enum {
    REG_DEVICE_CTRL = mlink::MregDeviceAdcm::ADCM16_CTRL_REG,        // 0x40
    REG_DEVICE_RLAT = mlink::MregDeviceAdcm::ADCM16_WIN_REG,         // 0x41
    REG_RUN_STATUS  = mlink::MregDeviceAdcm::ADCM16_STATUS_REG,      // 0x42
    REG_DEVICE_ID   = mlink::MregDeviceAdcm::ADCM16_STATUS_REG,      // 0x42
    REG_TRIG_CTRL   = mlink::MregDeviceAdcm::ADCM16_TRIGMULT_REG,    // 0x43
    REG_ADC_INFO    = 0x44,                                          // 0x44
    REG_CH_DPM_KS   = mlink::MregDeviceAdcm::ADCM16_CH_DPM_KS_REG,   // 0x4A
    REG_TEMPERATURE = mlink::MregDeviceAdcm::ADCM16_TEMPERATURE_REG, // 0x4B
    REG_FW_VER      = mlink::MregDeviceAdcm::ADCM16_FW_VER_REG,      // 0x4C
    REG_FW_REV      = mlink::MregDeviceAdcm::ADCM16_FW_REV_REG,      // 0x4D
    REG_SERIAL_NUM  = mlink::MregDeviceAdcm::ADCM16_SERIAL_ID_REG,   // 0x4E

    REG_DAC_MAX550_1 = 0x100, // 256
    REG_DAC_MAX550_2 = 0x101, // 257
    REG_PCA_12 = 0x102, // 258
    REG_ADC_SPI = 0x160, //352
    REG_ADC12_SPI_READ = 0x161, //353
    REG_ADC34_SPI_READ = 0x162, //354
    REG_ADC56_SPI_READ = 0x163, //355
    REG_ADC78_SPI_READ = 0x164, //356
    REG_ZS_EVENTS    = 0x110,  //(64 bits) 272,273,274,275

    // 0x160-0x16F - LTM9011 SPI devices(2 spi connections to 2 8ch adc)
    REG_LTM9011_SPI1_ADDR = 0x160, // 288
    REG_LTM9011_SPI1_WR_DATA = 0x161,
//    REG_LTM9011_SPI1_READ_DATA = 0x162,
    REG_LTM9011_SPI2_ADDR = 0x168, // 288
    REG_LTM9011_SPI2_WR_DATA = 0x169,
//    REG_LTM9011_SPI2_READ_DATA = 0x16A,

    // 0x120-0x123 - AD9252 SPI device #0
    REG_AD9252_SPI_ADDR = 0x120, // 288
    REG_AD9252_SPI_WR_DATA = 0x121,
    REG_AD9252_SPI_READ_DATA = 0x122,
    REG_AD9252_CSR = 0x123,
    // 0x123 reserved
    // 0x124-0x127 - AD9252 SPI device #1

    // 0x160-0x16F - AD9249 SPI devices(2 spi connections to 4 16ch adc)
    REG_AD9249_SPI_ADDR = 0x160, // 288
    REG_AD9249_SPI_WR_DATA = 0x161,
    REG_AD9249_SPI_READ_DATA = 0x162,
    REG_AD9249_CSR = 0x163,
    REG_AD9249_ACTIVE_ADC_SELECT = 0x164, //bits 0-3 adc12 , bits 4-7 adc34

    // 0x160-0x16F - ADS52J90 SPI devices(2 spi connections to 4 16ch adc)
    // Same as AD9249
    REG_ADS52J90_SPI_ADDR = 0x160, // 288
    REG_ADS52J90_SPI_WR_DATA = 0x161,
    REG_ADS52J90_SPI_READ_DATA = 0x162,
    REG_ADS52J90_CSR = 0x163,
    REG_ADS52J90_ACTIVE_ADC_SELECT = 0x164, //bits 0-3 adc12 , bits 4-7 adc34

    REG_AMP_SET = 0x130,
    REG_AD5622_COMMAND = 0x131,
    REG_AD5622_BASELINE = 0x132,
    REG_AD5622_LEVEL = 0x133,

    REG_MSTREAM_RUN_CTRL = 0x140,
    REG_MSTREAM_DATA_SIZE_BYTES = 0x141,
    REG_MSTREAM_READOUT_CHANNEL_EN = 0x142, //(64 bits) 322,323,324,325
    REG_MSTREAM_SPARSE_CTRL = 0x148,
    REG_MSTREAM_SPARSE_OFFSET = 0x149,
    REG_MSTREAM_SPARSE_PERIOD = 0x14A,
    REG_MSTREAM_MTU_SIZE = 0x14C,

    REG_DES_CTRL = 0x150,
    REG_DES_STATUS = 0x151,
    REG_DES_IDELAY_TAP_VAL = 0x153,
    REG_DES_IDELAY_LOAD_MASK = 0x154,

    REG_HMCAD1101_SPI_BASE = 0x160,
    REG_HMCAD1101_SPI_ADDR_REG = 0x168,

    REG_FIR_BASE = 0x200,
    REG_FIR_CONTROL = REG_FIR_BASE + 0,
    REG_FIR_COEF_CTRL = REG_FIR_BASE + 1,
    REG_FIR_ROUNDOFF = REG_FIR_BASE + 2,
    REG_FIR_COEF_START = REG_FIR_BASE + 0x10,

    REG_TRIG_CSR_BASE = 0x240,
    REG_TRIG_CSR_TRIG_TS = REG_TRIG_CSR_BASE + 0,
    REG_TRIG_CSR_EV_NUM = REG_TRIG_CSR_BASE + 4,
    REG_TRIG_CSR_TRIG_IN_DELAY = REG_TRIG_CSR_BASE + 8,
    REG_TRIG_CSR_TRIG_CODE = REG_TRIG_CSR_BASE + 9,

    REG_STATISTIC_CONTROL = 0x300,
    REG_ADC_STATUS = 0x301, // 16bit RO 0bit-adc mask ok; 1bit-WR time valid
    REG_RUN_EVENT_NUMBER = 0x302, // 32bit RO
    REG_WR_SYNC_LOST_COUNTER = 0x304, // 32bit RO
    REG_WR_LINK_ERROR_COUNTER = 0x306, // 32bit RO
    REG_ADC_STATUS_MASK = 0x308, // 32bit RW
    REG_TRIG_ON_XOFF_ERROR_COUNTER = 0x30A, // 32bit RO
    REG_RUN_EVENT_NUMBER_64 = 0x30C, // 64bit RO

    REG_ADC_TIME_SEC = 0x1000
};


enum {
    MEM_CH_WR_ADDR     = 0x0000,
    MEM_CH_CTRL        = 0x0001,
    MEM_CH_THR         = 0x0002,
    MEM_CH_ZS_THR      = 0x0003,
    MEM_CH_BASELINE    = 0x0004,
    MEM_CH_ADC_DATA    = 0x0005,
    MEM_CH_ADC_PATTERN = 0x0006,
    MEM_CH_ADC_PATTERN_MISMATCH_CNT = 0x0007,
    MEM_CH_BLC_THR_HI  = 0x0008,
    MEM_CH_BLC_THR_LO  = 0x0009,
    MEM_CH_D2_HIST     = 0x0080,
    MEM_CH_D2_HIST_CTRL= 0x00C0,
    MEM_CH_D2_HIST_ST  = 0x00C1,
    MEM_CH_D2_HIST_TIME= 0x00C2
};

enum {
    REG_DEVICE_CTRL_BIT_RUN = 0x0001,
    REG_DEVICE_CTRL_BIT_INC_BITCLK_DELAY = 0x0008
};

enum {
    REG_RUN_STATUS_BIT_SLAVE_0 = 0x0001,
    REG_RUN_STATUS_BIT_SLAVE_1 = 0x0002,
    REG_RUN_STATUS_BIT_SLAVE_2 = 0x0004,
    REG_RUN_STATUS_BIT_SLAVE_3 = 0x0008,
    REG_RUN_STATUS_BIT_RUNNING = 0x0010 //0x0001
};

typedef union {
    quint16 all;
    struct {
        unsigned int en: 1;              // [0]
        unsigned int invert: 1;          // [1]
        unsigned int thr_trig_invert: 1; // [2]
        unsigned int zs_thr_invert: 1;   // [3]
        unsigned int ch_thr_trig_en: 1;  // [4]
        unsigned int unused1: 3;
        unsigned int blc_out_sel1: 1;    // [8]
        unsigned int blc_out_sel2: 1;    // [9]
        unsigned int blc_maf_sel: 2;     // [11:10]
        unsigned int unused2: 4;
    } bit;
} reg_dig_ch_ctrl;

enum {
    MEM_CH_D2HIST_CTRL_BIT_EN         = 0x0001,
    MEM_CH_D2HIST_CTRL_BIT_CLR        = 0x0002
};

enum {
    ERC_BIT_EN    = 0x0001,
    ERC_BIT_ZS_EN = 0x0002,
};

enum {
    SPARSE_BIT_EN    = 0x0001,
    SPARSE_BIT_POINT_NUMBER = 0x0002,
    SPARSE_BIT_POINT_NUMBER2 = 0x0004,
};

enum {
    ADC_CLK_CTRL_BIT_INC            = 0x8000,
    ADC_CLK_CTRL_BIT_ADC_RESET      = 0x4000,
    ADC_CLK_CTRL_BIT_STATUS_RESET   = 0x2000,
    ADC_CLK_CTRL_BIT_SYNC_RESET     = 0x1000,
    ADC_CLK_CTRL_BIT_RESET          = 0x0100,
    ADC_CLK_CTRL_ALL_ADCS           = 0x00FF
};

enum {AD5622_WRITE_COMMAND = 0x180};

enum {
    FIR_ENABLE_BIT = 0x1,
};

#endif // DOMINODEVICEREGISTERS_H
