#include "DominoDeviceLtm9011.h"

#include "DominoDeviceRegisters.h"

namespace {
// const bool ADC_12_BIT_MODE = true;
const bool ADC_12_BIT_MODE = false;
// LTM9011
enum LTM9011_ADC_SPI_REG {
    LTM9011_ADC_SPI_A0_REG  = 0, // reset
    LTM9011_ADC_SPI_A1_REG  = 1, // format and power-down
    LTM9011_ADC_SPI_A2_REG  = 2, // output mode
    LTM9011_ADC_SPI_A3_REG  = 3, // test pattern msb
    LTM9011_ADC_SPI_A4_REG  = 4, // test pattern lsb
};
}

DominoDeviceLtm9011::DominoDeviceLtm9011(quint16 devId, quint64 devSerial, QObject *parent)
    : DominoDevice(devId, devSerial, parent)
{
    Q_ASSERT(getDeviceId() == DEVICE_ID_TQDC16VS_DIG);
}

void DominoDeviceLtm9011::setupAdc()
{
    int writeReg = REG_LTM9011_SPI1_WR_DATA;
    int adrReg = REG_LTM9011_SPI1_ADDR;

    // set deserializers to reset
    regWrite(REG_DES_CTRL, ADC_CLK_CTRL_BIT_ADC_RESET);

    for(int i=0;i<2;i++){
        regWrite(writeReg, 1<<7); // reset bit
        regWrite(adrReg, LTM9011_ADC_SPI_A0_REG);

////        ////    regWrite(LTM9011_ADC0_SPI_WRITE_REG, 0x40); // randomizer
        regWrite(writeReg, 0x20); // 2's complement
//        regWrite(writeReg, 0x40); // rnd
        regWrite(adrReg, LTM9011_ADC_SPI_A1_REG);

        if(ADC_12_BIT_MODE) {
            regWrite(writeReg, 0x2); // 2-lane 12-bit ser
            regWrite(adrReg, LTM9011_ADC_SPI_A2_REG);
        } else {
            regWrite(writeReg, 0x0); // 2-lane 16-bit ser
            regWrite(adrReg, LTM9011_ADC_SPI_A2_REG);
        }

//        regWrite(writeReg, 0x00);
//        regWrite(adrReg, LTM9011_ADC_SPI_A4_REG);
//        regWrite(writeReg, 0xbf);
//        regWrite(adrReg, LTM9011_ADC_SPI_A3_REG);

        adrReg += 8;
        writeReg += 8;
    }
//    setCustomPattern(0x01ff);

    // set deserializers to normal work
    regWrite(REG_DES_CTRL, 0);

//    ResetAdc();
    lockTest();
}

QMap<int, bool> DominoDeviceLtm9011::patternTestByChannel()
{
    quint16 spi_pattern;
    quint16 adc_pattern;
    QMap<int,bool> testok;

    for (int n=0; n<spiPatternSet.size(); ++n)
    {
        spi_pattern = spiPatternSet[n];
        spi_pattern &= 0x3FFF;
        setCustomPattern(spi_pattern);
        //arrange to 16bit data
        adc_pattern = spi_pattern << 2;
        if(ADC_12_BIT_MODE)
            adc_pattern &= ~0xFu;
        QMap<int,bool> test = TestAdcCustomPatternByChannel(adc_pattern);
        if(n==0) {
            testok = test;
        } else {
            foreach (int ch, test.keys())
                testok[ch]&=test[ch];
        }
    }
    return testok;
}

void DominoDeviceLtm9011::setCustomPatternMode(bool mode)
{
    if(mode) {
        setCustomPattern(0x200);
    } else {
        int writeReg = REG_LTM9011_SPI1_WR_DATA;
        int adrReg = REG_LTM9011_SPI1_ADDR;
        for(int i=0;i<2;i++){
            regWrite(writeReg, 0);
            regWrite(adrReg, LTM9011_ADC_SPI_A3_REG);

            adrReg += 8;
            writeReg += 8;
        }
    }
}

void DominoDeviceLtm9011::setCustomPattern(quint16 data)
{
    int writeReg = REG_LTM9011_SPI1_WR_DATA;
    int adrReg = REG_LTM9011_SPI1_ADDR;
    quint8 dataLow = data & 0xff;
    quint8 dataHi = (data >> 8) & 0x3fu;

    dataHi |= 0x80; // enable test pattern
    for(int i=0;i<2;i++){
        regWrite(writeReg, dataLow);
        regWrite(adrReg, LTM9011_ADC_SPI_A4_REG);
        regWrite(writeReg, dataHi);
        regWrite(adrReg, LTM9011_ADC_SPI_A3_REG);

        adrReg += 8;
        writeReg += 8;
    }
//    ResetAdc();
    lockTest();
}

