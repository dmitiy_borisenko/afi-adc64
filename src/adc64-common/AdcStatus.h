#ifndef ADCSTATUS_H
#define ADCSTATUS_H

#include <QMap>
#include <QMetaType>

#include "mregdev/BMStatus.h"
#include "mregdev/WrEpStatus.h"
#include "mregdev/fw_version.h"

class AdcStatus
{
public:
    QString fwStr; // Reg 0x56/0x57, FW_VER/FW_REV, 16-bit, firmware version/revision number, RO
    fw_version_t fw_ver;
    quint32 evNum = 0; // Reg 0x202, 32bit event number, RO
    WrEpStatus wrEpStatus;
    BMStatus bmStatus;
    quint32 trigOnXOff = 0; // Reg 0x30A
    quint16 adcStatus = 0; // Reg 0x301
    quint16 adcStatusMask = 0; // Reg 0x308

    bool liveMagicCorrect = true;
    quint16 adcDesStatus = 0;
    bool adcSelfTestOk = true;
    int liveMagicResetCount = 0;
    double temp = 0;
    bool mstreamVer2 = false;
    bool msMultiAck = false;
    quint8 hwBufSize = 8;
    quint8 adcBits = 12;
    quint8 adcPeriod = 16; // ns b/w bins

    AdcStatus & operator-=(const AdcStatus& st){
        evNum -= st.evNum;
        wrEpStatus -= st.wrEpStatus;
        liveMagicResetCount -= st.liveMagicResetCount;
        return *this;
    }
    void clear() {
        evNum=0;
        liveMagicCorrect = true;
        adcDesStatus = 0;
        adcSelfTestOk = true;
        liveMagicResetCount = 0;
        temp=0;
    }
};
Q_DECLARE_METATYPE(AdcStatus)

struct AdcClkStatus {
    struct WPoint {
        int x;
        int y;
        double weight;
        bool status;
    };

    WPoint getOptimalDelays() const;
    AdcClkStatus &operator&=(const AdcClkStatus &r);
    QString toString() const;

    QMap<int, QMap<int, bool > > data; // delayStatus <clockDelay, <dataDelay, isOk> >
    double temp = 0;
    QString fwStr;
};
Q_DECLARE_METATYPE(AdcClkStatus)

#endif // ADCSTATUS_H
