#include "DominoDeviceHMCAD1101.h"

#include "DominoDeviceRegisters.h"
#include "device_features.h"

DominoDeviceHMCAD1101::DominoDeviceHMCAD1101(quint16 devId, quint64 devSerial, QObject *parent)
    : DominoDevice(devId, devSerial, parent)
{
    Q_ASSERT(getDeviceId() == DEVICE_ID_ADC64S2 ||
             getDeviceId() == DEVICE_ID_ADC64WR);
}


void DominoDeviceHMCAD1101::setupAdc()
{
    ResetAdc();
    int reg46 = hasAdcRawDataSigned(status.fw_ver) ? 0x0004 : 0x0000;
    spi_write(0x46, reg46);
}

QMap<int, bool> DominoDeviceHMCAD1101::patternTestByChannel()
{
    quint16 spi_pattern;
    quint16 adc_pattern;
    QMap<int,bool> testok;

    for (int n=0; n<spiPatternSet.size(); ++n)
    {
        spi_pattern = spiPatternSet[n];
        spi_pattern &= 0xFFF; // 12-bit mask
        adc_pattern = (spi_pattern << 4);  //arrange to 16bit data
        setCustomPattern(adc_pattern);
        QMap<int,bool> test = TestAdcCustomPatternByChannel(adc_pattern);
        if(n==0) {
            testok = test;
        } else {
            foreach (int ch, test.keys())
                testok[ch]&=test[ch];
        }
    }
    return testok;
}

void DominoDeviceHMCAD1101::setCustomPatternMode(bool mode)
{
    spi_write(0x25, mode ? 0x10 : 0);
}

void DominoDeviceHMCAD1101::setCustomPattern(quint16 data)
{
    spi_write(0x26, data);
}

//bool DominoDeviceHMCAD1101::ramp_test()
//{
//    spi_write(0x25, 0x40);   //ramp enable
//    ResetAdcStatus();
//    for (int i=0; i<10; i++)    //dummy read for delay
//        regRead(REG_DES_STATUS);
//    quint16 adc_status = regRead(REG_DES_STATUS);
//    spi_write(0x25, 0x0);   //ramp disable

//    bool lock_ok = (adc_status & 0xFF) == 0xFF;
//    bool test_error = ((adc_status >> 8) & 0xFF) != 0;

//    adcSelfTestOk = lock_ok & !test_error;

//    emit adcLockStatusUpdated(getDeviceIndex(), lock_ok, adc_status & 0xFF);
//    emit adcRampStatusUpdated(getDeviceIndex(), !test_error, adc_status >> 8);
//    if (!lock_ok) qCritical() << "ADC data lock error. " << adcStatusStr(adc_status);
//    else
//    if (test_error) qCritical() << "ADC data test error. " << adcStatusStr(adc_status);
//    else
//        qDebug() << adcStatusStr(adc_status);

//    return lock_ok;
//}

void DominoDeviceHMCAD1101::spi_write(quint8 addr, quint16 data)
{
    for (int i=0; i<getDeserializeNum(); i++)
        regWrite(REG_HMCAD1101_SPI_BASE + i, data);
    regWrite(REG_HMCAD1101_SPI_ADDR_REG, addr);
}
