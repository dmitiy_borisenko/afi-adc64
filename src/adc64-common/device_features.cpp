/*
**    Copyright 2020 Ilja Slepnev
**
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "device_features.h"

static const fw_version_t REV_BLOCK_COUNTERS = fw_version_t(1, 0, 22722);
static const fw_version_t REV_FIX_RUN_ENABLE = REV_BLOCK_COUNTERS;
static const fw_version_t REV_MSTREAM_2_2 = fw_version_t(1, 0, 23020);
static const fw_version_t REV_MTU_SET = fw_version_t(1, 0, 23020);
static const fw_version_t REV_TWOSCOMP = fw_version_t(1, 0, 23232);
static const fw_version_t REV_STREAM_READ = fw_version_t(1, 0, 23936); // also fix sparse/2
static const fw_version_t REV_ADC_INFO = fw_version_t(1, 0, 27380);

bool hasRunEnableBitFix(const fw_version_t &v)
{
    if (v.isNull())
        return true;
    return v >= REV_FIX_RUN_ENABLE;
}

bool hasCountersLock(const fw_version_t &v)
{
    if (v.isNull())
        return true;
    return v >= REV_BLOCK_COUNTERS;
}

bool hasMstream22(const fw_version_t &v)
{
    if (v.isNull())
        return true;
    return v >= REV_MSTREAM_2_2;
}

bool hasMtuSetting(const fw_version_t &v)
{
    if (v.isNull())
        return true;
    return v >= REV_MTU_SET;
}

bool hasAdcRawDataSigned(const fw_version_t &v)
{
    if (v.isNull())
        return true;
    return v >= REV_TWOSCOMP;
}

bool hasStreamRead(const fw_version_t &v)
{
    if (v.isNull())
        return true;
    return v >= REV_STREAM_READ;
}

bool hasAdcInfo(const fw_version_t &v)
{
    if (v.isNull())
        return true;
    return v >= REV_ADC_INFO;
}
