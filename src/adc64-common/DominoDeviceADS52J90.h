#ifndef DOMINODEVICEADS52J90_H
#define DOMINODEVICEADS52J90_H

#include "dominodevice.h"

class DominoDeviceADS52J90 : public DominoDevice
{
public:
    DominoDeviceADS52J90(quint16 devId, quint64 devSerial, QObject *parent = nullptr);

protected:
    void setupAdc() override;
    QMap<int, bool> patternTestByChannel() override;
    void setCustomPatternMode(bool mode) override;
    void setCustomPattern(quint16 data) override;
    int getDeserializeNum() const override { return 4; }

private:
    void spi_write(quint16 addr, quint16 data, quint16 device_mask);
    quint16 spi_read(quint16 addr, quint16 device_mask);
    void spi_test();
};

#endif // DOMINODEVICEADS52J90_H
