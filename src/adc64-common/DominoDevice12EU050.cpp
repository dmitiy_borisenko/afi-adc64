#include "DominoDevice12EU050.h"

#include "DominoDeviceRegisters.h"

namespace {
const int ADC_BITS = 12;
const int ADC_HALF_VALUE = 1<<(ADC_BITS-1);
const int ADC_MASK = (1<<ADC_BITS) - 1;
enum {
    ADC_SPI_PATTERN_LOW_REG   = 0x10,
    ADC_SPI_PATTERN_HIGH_REG  = 0x12,
    ADC_SPI_DECIMATOR_CTRL_REG = 0x16
};
}

DominoDevice12EU050::DominoDevice12EU050(quint16 devId, quint64 devSerial, QObject *parent)
    : DominoDevice(devId, devSerial, parent)
{
    Q_ASSERT(getDeviceId() == DEVICE_ID_ADC64);
}

QMap<int, bool> DominoDevice12EU050::patternTestByChannel()
{
    quint16 spi_pattern;
    quint16 adc_pattern;
    QMap<int, bool> testok;

    for (int n=0; n<spiPatternSet.size(); ++n)
    {
        spi_pattern = spiPatternSet[n];
        spi_pattern &= 0xFFF;
        setCustomPattern(spi_pattern);

        adc_pattern = ((spi_pattern ^ ADC_HALF_VALUE) << (16 - ADC_BITS));  //arrange to 16bit data
        QMap<int, bool> test = TestAdcCustomPatternByChannel(adc_pattern);
        if(n==0) {
            testok = test;
        } else {
            foreach (int ch, test.keys())
                testok[ch]&=test[ch];
        }
    }
    return testok;
}

void DominoDevice12EU050::setCustomPattern(quint16 data)
{
    //    qDebug() << "Setting pattern" << hex << data;
    data &= ADC_MASK;
    spi_write(ADC_SPI_PATTERN_LOW_REG, (data&0xFF), false);
    spi_write(ADC_SPI_PATTERN_HIGH_REG, (data>>8), false);
}

void DominoDevice12EU050::setCustomPatternMode(bool mode)
{
    if (mode) spi_write(ADC_SPI_DECIMATOR_CTRL_REG, 0x3, true);
    else  spi_write(ADC_SPI_DECIMATOR_CTRL_REG, 0, true);
}

bool DominoDevice12EU050::spi_write(quint8 reg, quint8 data, bool readback)
{
    quint16 senddata = 0;
    senddata = (reg<<8) | data;
    regWrite(REG_ADC_SPI, senddata);

    if (!readback) return true; //Write complete, start Read if needed

    int errnum = 0;
    bool testok  = true;
    QVector<quint16> spi_rd;
    const int MAX_ERR_NUM = 16;

    do
    {
        senddata = ((reg+1)<<8);  //write even register, read odd register (read=write+1 reg number)
        regWrite(REG_ADC_SPI, senddata);
        spi_rd.clear();
        spi_rd.push_back(regRead(REG_ADC12_SPI_READ));
        spi_rd.push_back(regRead(REG_ADC34_SPI_READ));
        spi_rd.push_back(regRead(REG_ADC56_SPI_READ));
        spi_rd.push_back(regRead(REG_ADC78_SPI_READ));
        testok = true;
        for (int i=0; i<spi_rd.size(); i++)
        {
            if ((data!=(spi_rd[i]&0xFF)) || (data!=((spi_rd[i]>>8)&0xFF)))
            {
                //                qDebug() << "SPI R/W FAIL cycle " << errnum << "i=" << i << "data=" << hex << data << " spi_rd[i]=" << hex << spi_rd[i];
                errnum++;
                testok = false;
            }
            else
            {
                //                qDebug() << "SPI R/W OK, i=" << i << " data = " << hex << data;
                testok &= true;
            }
        }
    } while (!testok && errnum < MAX_ERR_NUM);
    if (errnum >= MAX_ERR_NUM) qCritical() << "More ERRORS!!!";
    else qDebug() << "SPI R/W OK";
    return testok;
}

QVector<quint16> DominoDevice12EU050::spi_read(quint8 reg)
{
    QVector<quint16> spi_rd;
    quint16 senddata = 0;
    senddata = ((reg+1)<<8);  //write even register, read odd register (read=write+1 reg number)
    regWrite(REG_ADC_SPI, senddata);
    spi_rd.clear();
    spi_rd.push_back(regRead(REG_ADC12_SPI_READ));
    spi_rd.push_back(regRead(REG_ADC34_SPI_READ));
    spi_rd.push_back(regRead(REG_ADC56_SPI_READ));
    spi_rd.push_back(regRead(REG_ADC78_SPI_READ));
    qDebug() << "SPI Read " << hex << spi_rd;
    return spi_rd;
}

