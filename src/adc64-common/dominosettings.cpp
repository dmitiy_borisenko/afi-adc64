//
//    Copyright 2010 Alexey Shutov, Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "dominosettings.h"

#include <QSettings>

#include "mldiscover/DeviceDescription.h"
#include "mongo/RootConfig.h"

const QString CHANNELS_ENABLE = "channels_enable";
const QString CHANNELS_TRIGGER = "channels_trigger";
const QString CHANNELS_TRIGGER_ENABLE = "channels_trigger_enable";
const QString CHANNELS_ZERRO_SUPRESSION = "channels_zerro_supression";
const QString SPARSE = "sparse";
const QString DSP = "dsp";
const QString FIR = "fir";
const QString BLC = "blc";
const QString MAF = "maf";
const QString CHANNELS_BASELINE = "channels_baseline";
const QString TRIGGER_SETUP = "trigger_setup";
const QString BIT = "bit";
const QString REG_PCA = "reg_pca";

namespace {
const int FIR_ROUNDOFF_DEFAULT = 0x0001;
const QString SECTION_PREFIX_FIR = "FIR_";
const QString SECTION_PREFIX_SETUP = "Setup_";
}

FirParams::FirParams()
    : roundoff(FIR_ROUNDOFF_DEFAULT)
{
    coef.resize(16);
    coef[0] = 32767;
}

void FirParams::setRoundoff(int value)
{
    if (value < 0)
        value = 0;
    if (value > getRoundoffMax())
        value = getRoundoffMax();
    roundoff = value;
}

DominoSettings::DominoSettings()
    // EVADC only
    // writecellnumber(1024),
    // analogDelay(1600)
{
    trigsetup.all = 0;
    reg_pca.all = 0;
}

void DominoSettings::root_to_values(RootConfig root)
{
    device_id = root.get_data<quint16>("device_id");
    device_serial = root.get_data<quint32>("device_serial");
    serial = root.get_data<QString>("serial");
    readCellNumber = root.get_data<int>("readCellNumber");
    trigDelay = root.get_data<int>("trigDelay");
    invertInput = root.get_data<bool>("invertInput");
    zs_en = root.get_data<bool>("zs_en");
    softwareZs = root.get_data<bool>("softwareZs");
    ad5622op.data_baseline = root.get_data<quint16>("ad5622op_data_baseline");
    ad5622op.data_level = root.get_data<quint16>("ad5622op_data_level");
    amp_board_set = root.get_data<quint16>("amp_board_set");
    invert_thr_trig = root.get_data<bool>("invert_thr_trig");
    invert_zs_thr = root.get_data<bool>("invert_zs_thr");
    hwStr = root.get_data<QString>("hwStr");
    enabled = root.get_data<bool>("enabled");

    devIndex = DeviceIndex(device_id, device_serial);

    for(const QString &child_name : root.children().keys()){
        if(child_name == TRIGGER_SETUP){
            RootConfig child = root.child(child_name);
            trigsetup.all = child.get_data<quint16>("all");
            for(const QString &grandson_name : child.children().keys()){
                if(grandson_name == BIT){
                    trigsetup.bit.timer_en = child.child(grandson_name).get_data<bool>("timer_en");
                    trigsetup.bit.threshold_en = child.child(grandson_name).get_data<bool>("threshold_en");
                    trigsetup.bit.ttl_en = child.child(grandson_name).get_data<bool>("ttl_en");
                    trigsetup.bit.unused2 = child.child(grandson_name).get_data<unsigned int>("unused2");
                }
            }
        }

        if(child_name == REG_PCA){
            RootConfig child = root.child(child_name);
            reg_pca.all = child.get_data<quint16>("all");
            for(const QString &grandson_name : child.children().keys()){
                if(grandson_name == BIT){
                    reg_pca.bit.polarity1 = child.child(grandson_name).get_data<bool>("polarity1");
                    reg_pca.bit.shaper1_1 = child.child(grandson_name).get_data<bool>("shaper1_1");
                    reg_pca.bit.shaper1_2 = child.child(grandson_name).get_data<bool>("shaper1_2");
                    reg_pca.bit.shaper1_3 = child.child(grandson_name).get_data<bool>("shaper1_3");
                    reg_pca.bit.gain1_1 = child.child(grandson_name).get_data<bool>("gain1_1");
                    reg_pca.bit.gain1_2 = child.child(grandson_name).get_data<bool>("gain1_2");
                    reg_pca.bit.preamp_en1 = child.child(grandson_name).get_data<bool>("preamp_en1");
                    reg_pca.bit.unused1 = child.child(grandson_name).get_data<bool>("unused1");
                    reg_pca.bit.polarity2 = child.child(grandson_name).get_data<bool>("polarity2");
                    reg_pca.bit.shaper2_1 = child.child(grandson_name).get_data<bool>("shaper2_1");
                    reg_pca.bit.shaper2_2 = child.child(grandson_name).get_data<bool>("shaper2_2");
                    reg_pca.bit.shaper2_3 = child.child(grandson_name).get_data<bool>("shaper2_3");
                    reg_pca.bit.gain2_1 = child.child(grandson_name).get_data<bool>("gain2_1");
                    reg_pca.bit.gain2_2 = child.child(grandson_name).get_data<bool>("gain2_2");
                    reg_pca.bit.preamp_en2 = child.child(grandson_name).get_data<bool>("preamp_en2");
                    reg_pca.bit.unused2 = child.child(grandson_name).get_data<bool>("unused2");
                }
            }
        }

        if(child_name == CHANNELS_ENABLE){
            RootConfig child = root.child(child_name);
            for(const QString &field : child.data().keys()){
                chEn[field.toInt()] = child.get_data<bool>(field);
            }
        }

        if(child_name == CHANNELS_TRIGGER){
            RootConfig child = root.child(child_name);
            for(const QString &field : child.data().keys()){
                chTrigThr[field.toInt()] = child.get_data<int>(field);
            }
        }

        if(child_name == CHANNELS_ZERRO_SUPRESSION){
            RootConfig child = root.child(child_name);
            for(const QString &field : child.data().keys()){
                chZsThr[field.toInt()] = child.get_data<int>(field);
            }
        }

        if(child_name == CHANNELS_TRIGGER_ENABLE){
            RootConfig child = root.child(child_name);
            for(const QString &field : child.data().keys()){
                chTrigEn[field.toInt()] = child.get_data<bool>(field);
            }
        }

        if(child_name == SPARSE){
            RootConfig child = root.child(child_name);
            sparseParams.en = child.get_data<bool>("en");
            sparseParams.readCellNum = child.get_data<quint16>("readCellNum");
            sparseParams.offset = child.get_data<quint16>("offset");
            sparseParams.period = child.get_data<quint16>("period");
            sparseParams.pointNumber = child.get_data<quint8>("pointNumber");
        }

        if(child_name == DSP){
            RootConfig child_dsp = root.child(child_name);
            dspParams.en = child_dsp.get_data<bool>("en");
                RootConfig child_fir = child_dsp.child(FIR);
                dspParams.fir.en = child_fir.get_data<bool>("en");
                dspParams.fir.roundoff = child_fir.get_data<int>("roundoff");
                dspParams.fir.presetKey = child_fir.get_data<QString>("presetKey");

                RootConfig child_blc = child_dsp.child(BLC);
                dspParams.blc_thr = child_blc.get_data<int>("thr");
                    RootConfig child_maf = child_blc.child(MAF);
                    dspParams.maf_enable = child_maf.get_data<bool>("en");
                    dspParams.maf_tap_sel = child_maf.get_data<int>("tap_sel");
        }

        if(child_name == CHANNELS_BASELINE){
            RootConfig child_baseline = root.child(child_name);
            for(const QString &field : child_baseline.data().keys()){
                chBaseline[field.toInt()] = child_baseline.get_data<int>(field);
            }
        }
    }
}

RootConfig DominoSettings::values_to_root()
{
    RootConfig root;
    device_id = devIndex.getDevId();
    device_serial = devIndex.getSerial();
    serial = devIndex.getSerialStr();

    root.set_data("device_id", device_id);
    root.set_data("device_serial", device_serial);
    root.set_data("serial", serial);
    root.set_data("readCellNumber", readCellNumber);
    root.set_data("trigDelay", trigDelay);
    root.set_data("invertInput", invertInput);
    root.set_data("zs_en", zs_en);
    root.set_data("softwareZs", softwareZs);
    root.set_data("ad5622op_data_baseline", ad5622op.data_baseline);
    root.set_data("ad5622op_data_level", ad5622op.data_level);
    root.set_data("amp_board_set", amp_board_set);
    root.set_data("invert_thr_trig", invert_thr_trig);
    root.set_data("invert_zs_thr", invert_zs_thr);
    root.set_data("hwStr", hwStr);
    root.set_data("enabled", enabled);

    RootConfig root_trigsetup(TRIGGER_SETUP);
    root_trigsetup.set_data("all", trigsetup.all);
    RootConfig root_trigsetup_bit(BIT);
        root_trigsetup_bit.set_data("timer_en", trigsetup.bit.timer_en);
        root_trigsetup_bit.set_data("threshold_en", trigsetup.bit.threshold_en);
        root_trigsetup_bit.set_data("ttl_en", trigsetup.bit.ttl_en);
        root_trigsetup_bit.set_data("unused2", trigsetup.bit.unused2);
    root_trigsetup.append_child(root_trigsetup_bit);
    root.append_child(root_trigsetup);

    RootConfig root_reg_pca(REG_PCA);
    root_reg_pca.set_data("all", reg_pca.all);
    RootConfig root_reg_pca_bit(BIT);
        root_reg_pca_bit.set_data("polarity1", reg_pca.bit.polarity1);
        root_reg_pca_bit.set_data("shaper1_1", reg_pca.bit.shaper1_1);
        root_reg_pca_bit.set_data("shaper1_2", reg_pca.bit.shaper1_2);
        root_reg_pca_bit.set_data("shaper1_3", reg_pca.bit.shaper1_3);
        root_reg_pca_bit.set_data("gain1_1", reg_pca.bit.gain1_1);
        root_reg_pca_bit.set_data("gain1_2", reg_pca.bit.gain1_2);
        root_reg_pca_bit.set_data("preamp_en1", reg_pca.bit.preamp_en1);
        root_reg_pca_bit.set_data("unused1", reg_pca.bit.unused1);
        root_reg_pca_bit.set_data("polarity2", reg_pca.bit.polarity2);
        root_reg_pca_bit.set_data("shaper2_1", reg_pca.bit.shaper2_1);
        root_reg_pca_bit.set_data("shaper2_2", reg_pca.bit.shaper2_2);
        root_reg_pca_bit.set_data("shaper2_3", reg_pca.bit.shaper2_3);
        root_reg_pca_bit.set_data("gain2_1", reg_pca.bit.gain2_1);
        root_reg_pca_bit.set_data("gain2_2", reg_pca.bit.gain2_2);
        root_reg_pca_bit.set_data("preamp_en2", reg_pca.bit.preamp_en2);
        root_reg_pca_bit.set_data("unused2", reg_pca.bit.unused2);
    root_reg_pca.append_child(root_reg_pca_bit);
    root.append_child(root_reg_pca);


    RootConfig root_channels(CHANNELS_ENABLE);
    for(int i : chEn.keys()){
        QString key = QString::number(i);
        if(i<10){
            key.prepend("0");
        }
        root_channels.set_data(key, chEn[i]);
    }
    root.append_child(root_channels);

    RootConfig root_trigger(CHANNELS_TRIGGER);
    for(int i : chTrigThr.keys()){
        QString key = QString::number(i);
        if(i<10){
            key.prepend("0");
        }
        root_trigger.set_data(key, chTrigThr[i]);
    }
    root.append_child(root_trigger);

    RootConfig root_zs(CHANNELS_ZERRO_SUPRESSION);
    for(int i : chZsThr.keys()){
        QString key = QString::number(i);
        if(i<10){
            key.prepend("0");
        }
        root_zs.set_data(key, chZsThr[i]);
    }
    root.append_child(root_zs);

    RootConfig root_ch_trig_en(CHANNELS_TRIGGER_ENABLE);
    for(int i : chTrigEn.keys()){
        QString key = QString::number(i);
        if(i<10){
            key.prepend("0");
        }
        root_ch_trig_en.set_data(key, chTrigEn[i]);
    }
    root.append_child(root_ch_trig_en);

    RootConfig root_sparse(SPARSE);
        root_sparse.set_data("en", sparseParams.en);
        root_sparse.set_data("readCellNum", sparseParams.readCellNum);
        root_sparse.set_data("offset", sparseParams.offset);
        root_sparse.set_data("period", sparseParams.period);
        root_sparse.set_data("pointNumber", sparseParams.pointNumber);
    root.append_child(root_sparse);

    RootConfig root_dsp(DSP);
        root_dsp.set_data("en", dspParams.en);
        RootConfig root_fir(FIR);
            root_fir.set_data("en", dspParams.fir.en);
            root_fir.set_data("roundoff", dspParams.fir.roundoff);
            root_fir.set_data("presetKey", dspParams.fir.presetKey);
        root_dsp.append_child(root_fir);
        RootConfig root_blc(BLC);
            root_blc.set_data("thr", dspParams.blc_thr);
                RootConfig root_maf(MAF);
                    root_maf.set_data("en", dspParams.maf_enable);
                    root_maf.set_data("tap_sel", dspParams.maf_tap_sel);
                root_blc.append_child(root_maf);
        root_dsp.append_child(root_blc);
    root.append_child(root_dsp);

    RootConfig root_ch_baseline(CHANNELS_BASELINE);
    for(int i : chBaseline.keys()){
        QString key = QString::number(i);
        if(i<10){
            key.prepend("0");
        }
        root_ch_baseline.set_data(key, chBaseline[i]);
    }
    root.append_child(root_ch_baseline);

    return root;
}

void DominoSettings::copyFrom(const DominoSettings &ds)
{
    DominoSettings tmp = ds;
    tmp.devIndex = devIndex;
    tmp.hwStr = hwStr;
    tmp.chBaseline = chBaseline;
    tmp.enabled = enabled;
    tmp.nch = nch;

    *this = tmp;
}

void DominoSettings::setNCh(int n)
{
    nch = n;
    for(int i=0; i<n; i++) {
        if (!chEn.contains(i))
            chEn[i] = true;
        if (!chBaseline.contains(i))
            chBaseline[i] = 0;
        if (!chTrigEn.contains(i))
            chTrigEn[i] = true;
        if (!chTrigThr.contains(i))
            chTrigThr[i] = 100;
        if (!chZsThr.contains(i))
            chZsThr[i] = -0x8000;
    }
}

void DominoSettings::readFirPreset(const QString &firPreset, QSettings &s)
{
    const QString sectionKey = QString(SECTION_PREFIX_FIR).append(firPreset);

    if(!firPreset.isEmpty() && s.childGroups().contains(sectionKey)){
        dspParams.fir.presetKey = firPreset;
        s.beginGroup(sectionKey);
        for(int i=0;;i++) {
            QString key = QString("coef.%1").arg(i);
            if (!s.contains(key)) break;
            dspParams.fir.coef[i] = s.value(key).toInt();
        }
        s.endGroup();
    } else {
        // Set fir coef by default
        dspParams.fir.presetKey = "";
        FirParams fp;
        dspParams.fir.coef = fp.coef;
    }
}

void DominoSettings::set_en_all(bool state)
{
    for(int i=0; i<nch; i++)
        chEn[i] = state;
}

void DominoSettings::setInvert(bool invert)
{
    if(invertInput == invert)
        return;

    invertInput = invert;
    invert_thr_trig = !invert_thr_trig;
    invert_zs_thr = !invert_zs_thr;
    foreach (int ch, chBaseline.keys())
        chBaseline[ch] = chBaseline[ch]*(-1);
    foreach (int ch, chTrigThr.keys())
        chTrigThr[ch] = chTrigThr[ch]*(-1);
    foreach (int ch, chZsThr.keys())
        chZsThr[ch] = chZsThr[ch]*(-1);
}

QString DominoSettings::getSettingsKey() const
{
    return QString("%1%2_%3").arg(SECTION_PREFIX_SETUP)
            .arg(devIndex.getSerial(), 8, 16, QChar('0'))
            .arg(devIndex.getDevId(), 2, 16, QChar('0'));
}

DeviceIndex DominoSettings::getIndexBySettingsKey(const QString &key)
{
    DeviceIndex i;
    QStringList subStr = key.split('_');
    if(subStr.size()==3 && subStr.first() == "Setup"){
        bool ok;
        quint64 ser = subStr[1].toULongLong(&ok, 16);
        if(!ok) return i;
        quint16 id = subStr[2].toUShort(&ok, 16);
        if(ser && id && ok) {
            i.first = id;
            i.second = ser;
        }
    }
    return i;
}

QStringList DominoSettings::getFirPresets(QSettings &s)
{
    QStringList presets;
    foreach (const QString &section, s.childGroups()) {
        if(section.startsWith(SECTION_PREFIX_FIR))
            presets << QString(section).remove(0, SECTION_PREFIX_FIR.length());
    }
    return presets;
}

QString DominoSettings::toString() const
{
    QStringList lines;
    QString chMaskStr;
    QPair<int,int> range;
    range.first = -1;
    int enabledChCnt=0;
    foreach (int ch, chEn.keys()) {
        if(chEn[ch]) {
            ++enabledChCnt;
//            chMask.append(" ").append(QString::number(ch));
            if(range.first == -1) {
                //first range
                range.first=ch;
                range.second=ch;
            } else if(range.second+1 == ch) {
                // continuation of range
                range.second=ch;
            } else {
                // new range
                if(range.first == range.second)
                    chMaskStr.append(QString(" %1").arg(range.first));
                else
                    chMaskStr.append(QString(" %1-%2").arg(range.first).arg(range.second));
                range.first=ch;
                range.second=ch;
            }
        }
    }
    // Flush last range if exists
    if(range.first != -1) {
        if(range.first == range.second)
            chMaskStr.append(" ").append(QString::number(range.first));
        else
            chMaskStr.append(QString(" %1-%2").arg(range.first).arg(range.second));
    }

//    qDebug()<<QString("number of ch:%1").arg(dominoSettingnch);
    lines<<QString("number of ch:%1").arg(nch);

//    qDebug()<<"chMask:"<<chMask;
    lines<<QString("chMask:%1; total:%2 channels").arg(chMaskStr).arg(enabledChCnt);

    QString trig("trigger:");
    if(trigsetup.bit.ttl_en)
        trig.append("ttl ");
    if(trigsetup.bit.threshold_en) {
        QString thigCh;
        foreach (int ch, chEn.keys()) {
            if(chEn[ch] && chTrigEn[ch]) {
                thigCh.append(QString("%1:%2 ").arg(ch).arg(chTrigThr[ch]));
            }
        }
        trig.append(QString("thr (%1)").arg(thigCh.trimmed()));
    }
    if(trigsetup.bit.timer_en)
        trig.append("timer ");
//    qDebug()<<trig;
    lines<<trig;

    switch (amp_board_set) {
    case AMP_BOARD_NONE:
//        qDebug()<<"amp: NONE";
        lines<<"amp: NONE";
        break;
    case AMP_BOARD_PCA:
//        qDebug()<<"amp: PCA";
        lines<<"amp: PCA";
        break;
    case AMP_BOARD_AD5622:
        qDebug()<<"amp: AD5622";
        lines<<"amp: AD5622";
//        qDebug()<<QString("ad5622 setup: baseline=%1; level=%2")
//                  .arg(dominoSettingad5622op.data_baseline)
//                  .arg(dominoSettingad5622op.data_level);
        lines<<QString("ad5622 setup: baseline=%1; level=%2")
                  .arg(ad5622op.data_baseline)
                  .arg(ad5622op.data_level);
        break;
    case AMP_BOARD_AD5622v2:
//        qDebug()<<"amp: AD5622v2";
//        qDebug()<<QString("ad5622 setup: baseline=%1; level=%2")
//                  .arg(dominoSettingad5622op.data_baseline)
//                  .arg(dominoSettingad5622op.data_level);
        lines<<"amp: AD5622v2";
        lines<<QString("ad5622 setup: baseline=%1; level=%2")
                  .arg(ad5622op.data_baseline)
                  .arg(ad5622op.data_level);
        break;
    default:
        break;
    }

//    qDebug()<<"readcellnumber:"<<dominoSettingreadcellnumber;
    lines<<QString("read cellNumber:%1; trigDelay:%2")
           .arg(sparseParams.en ?
                    QString::number(sparseParams.readCellNum).append("(sparse)")
                  : QString::number(readCellNumber))
           .arg(trigDelay);

    QString sparseStr("Sparse:");
    if(sparseParams.en){
        sparseStr.append(QString("offset:%1; period:%2; pointNum:%3")
                         .arg(sparseParams.offset)
                         .arg(sparseParams.period)
                         .arg(sparseParams.pointNumber));
    } else
        sparseStr.append("disabled");
    lines<<sparseStr;

    QString dspStr("DSP:");
    if(dspParams.en){
        const QString mafStr = QString("tapSel:%1,blcThr:%2").arg(dspParams.maf_tap_sel).arg(dspParams.blc_thr);
        dspStr.append(QString("roundoff:%1; fir:%2; maf:%3; %4")
                         .arg(dspParams.fir.roundoff)
                         .arg(dspParams.fir.en?dspParams.fir.presetKey:"disabled")
                      .arg(dspParams.maf_enable?mafStr : "disabled"));
        if(dspParams.test_enable)
            dspStr.append(" (test enabled)");
    } else
        dspStr.append("disabled");
    lines<<dspStr;

    return lines.join("\r");
}

int DominoSettings::getHwVer() const
{
    return DeviceDescription::getHwMajVer(hwStr);
}

// === Garbage ===

