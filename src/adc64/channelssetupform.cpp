//
//    Copyright 2011 Alexey Shutov
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "channelssetupform.h"

#include <QCheckBox>
#include <QDebug>
#include <QPushButton>
#include <QSpinBox>

#include "Config.h"
#include "adc64-common/dominosettings.h"
#include "ui_channelssetupform.h"

namespace  {
enum {COL_EN, COL_THRESHOLD_EN, COL_BASELINE, COL_TRIG_THR, COL_ZS_THR, COL_NUM };
const int THRESHOLD_SINGLE_STEP = 16;
const int BASELINE_SINGLE_STEP = 1;
}

ChannelsSetupForm::ChannelsSetupForm(Config *config, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChannelsSetupForm),
    config(config),
    tableWidgetChangeEnable(false)
{
    ui->setupUi(this);
    ui->tableWidgetSetup->setColumnCount(COL_NUM);
    ui->tableWidgetSetup->setRowCount(1);
    ui->tableWidgetSetup->setHorizontalHeaderItem(COL_EN, new QTableWidgetItem("Enable"));
    ui->tableWidgetSetup->setHorizontalHeaderItem(COL_THRESHOLD_EN, new QTableWidgetItem("Thr On/Off"));
    ui->tableWidgetSetup->setHorizontalHeaderItem(COL_BASELINE, new QTableWidgetItem("Baseline"));
    ui->tableWidgetSetup->setHorizontalHeaderItem(COL_TRIG_THR, new QTableWidgetItem("Trig thr"));
    ui->tableWidgetSetup->setHorizontalHeaderItem(COL_ZS_THR, new QTableWidgetItem("ZS thr"));
    connect(ui->tableWidgetSetup, SIGNAL(cellChanged(int, int)), this, SLOT(onTableWidgetChanged(int , int)));

    QTableWidgetItem *newitem = new QTableWidgetItem("Set/clear all");
    newitem->setCheckState(Qt::Checked);
    newitem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
    ui->tableWidgetSetup->setItem(0, COL_EN, newitem);

    newitem = new QTableWidgetItem("Set/clear all");
    newitem->setCheckState(Qt::Checked);
    newitem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
    ui->tableWidgetSetup->setItem(0, COL_THRESHOLD_EN, newitem);

    auto *spinBoxThr = new QSpinBox(ui->tableWidgetSetup);
    spinBoxThr->setMinimum(-0x8000);
    spinBoxThr->setMaximum(0x7fff);
    spinBoxThr->setSingleStep(THRESHOLD_SINGLE_STEP);
    connect(spinBoxThr, SIGNAL(valueChanged(int)), SLOT(onThresholdChanged(int)));
    ui->tableWidgetSetup->setCellWidget(0, COL_TRIG_THR, spinBoxThr);

    auto *spinBoxZsThr = new QSpinBox(ui->tableWidgetSetup);
    spinBoxZsThr->setMinimum(-0x8000);
    spinBoxZsThr->setMaximum(0x7fff);
    spinBoxZsThr->setSingleStep(THRESHOLD_SINGLE_STEP);
    connect(spinBoxZsThr, SIGNAL(valueChanged(int)), SLOT(onThresholdChanged(int)));
    ui->tableWidgetSetup->setCellWidget(0, COL_ZS_THR, spinBoxZsThr);

    auto *baseline = new QTableWidgetItem();
    baseline->setFlags(baseline->flags() ^ Qt::ItemIsEditable);
    ui->tableWidgetSetup->setItem(0, COL_BASELINE, baseline);

    ui->tableWidgetSetup->resizeColumnsToContents();
    ui->tableWidgetSetup->resizeRowsToContents();
    tableWidgetChangeEnable = true;
}

ChannelsSetupForm::~ChannelsSetupForm()
{
    delete ui;
}

void ChannelsSetupForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void ChannelsSetupForm::onSetSettings()
{
    onDeviceChange(selectedDevice);
}

void ChannelsSetupForm::onDeviceChange(const DeviceIndex &di)
{
    selectedDevice = di;
    if(config->device_configs.contains(selectedDevice))
        setNumberOfChannels(di, config->device_configs[selectedDevice].nch);
    else
        ui->tableWidgetSetup->setRowCount(1);
    restoreFormSettings();
    setWindowTitle(QString("Channels setup (%1)").arg(di.getSerialStr()));
}

void ChannelsSetupForm::setNumberOfChannels(DeviceIndex index, int num)
{
    if(!config->device_configs.contains(index))
        return;
    if(index!=selectedDevice)
        return;
    if(ui->tableWidgetSetup->rowCount() == num+1)
        return;
    bool bak = tableWidgetChangeEnable;
    tableWidgetChangeEnable = false;

    ui->tableWidgetSetup->setRowCount(num+1); //1 for set/clear all button
    QStringList row_names;
    row_names.append("Global");

    for (int row=1; row<num+1; row++)
    {
        auto *newitem = new QTableWidgetItem();
        newitem->setCheckState(Qt::Checked);
        newitem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
        ui->tableWidgetSetup->setItem(row, COL_EN, newitem);

        newitem = new QTableWidgetItem();
        newitem->setCheckState(Qt::Checked);
        newitem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
        ui->tableWidgetSetup->setItem(row, COL_THRESHOLD_EN, newitem);

        auto *spinBoxThr = new QSpinBox(ui->tableWidgetSetup);
        spinBoxThr->setMinimum(-0x8000);
        spinBoxThr->setMaximum(0x7fff);
        spinBoxThr->setSingleStep(THRESHOLD_SINGLE_STEP);
        connect(spinBoxThr, SIGNAL(valueChanged(int)), SLOT(onThresholdChanged(int)));
        ui->tableWidgetSetup->setCellWidget(row, COL_TRIG_THR, spinBoxThr);

        auto *spinBoxZSThr = new QSpinBox(ui->tableWidgetSetup);
        spinBoxZSThr->setMinimum(-0x8000);
        spinBoxZSThr->setMaximum(0x7fff);
        spinBoxZSThr->setSingleStep(THRESHOLD_SINGLE_STEP);
        connect(spinBoxZSThr, SIGNAL(valueChanged(int)), SLOT(onThresholdChanged(int)));
        ui->tableWidgetSetup->setCellWidget(row, COL_ZS_THR, spinBoxZSThr);

        auto *baseline = new QTableWidgetItem();
        baseline->setFlags(baseline->flags() ^ Qt::ItemIsEditable);
        ui->tableWidgetSetup->setItem(row, COL_BASELINE, baseline);

        row_names.append(QString("ch %1").arg(row));
    }
    ui->tableWidgetSetup->setVerticalHeaderLabels(row_names);
    restoreFormSettings();
    tableWidgetChangeEnable = bak;
    ui->tableWidgetSetup->resizeColumnsToContents();
    ui->tableWidgetSetup->resizeRowsToContents();
}

void ChannelsSetupForm::restoreFormSettings()
{
    if(!config->device_configs.contains(selectedDevice))
        return;
    const DominoSettings &settings = config->device_configs[selectedDevice];
    bool bak = tableWidgetChangeEnable;
    tableWidgetChangeEnable = false;
    for(int ch=0; ch<settings.nch; ++ch){
        if (settings.chEn.contains(ch))
            ui->tableWidgetSetup->item(ch+1, COL_EN)->setCheckState(settings.chEn[ch] ? Qt::Checked : Qt::Unchecked);
        if (settings.chTrigEn.contains(ch))
            ui->tableWidgetSetup->item(ch+1, COL_THRESHOLD_EN)->setCheckState(settings.chTrigEn[ch] ? Qt::Checked : Qt::Unchecked);
        if (settings.chTrigThr.contains(ch)){
            auto *sb = qobject_cast<QSpinBox *>(ui->tableWidgetSetup->cellWidget(ch+1, COL_TRIG_THR));
            Q_ASSERT(sb);
            sb->setValue(settings.chTrigThr[ch]);
        }
        if (settings.chZsThr.contains(ch)){
            auto *sb = qobject_cast<QSpinBox *>(ui->tableWidgetSetup->cellWidget(ch+1, COL_ZS_THR));
            Q_ASSERT(sb);
            sb->setValue(settings.chZsThr[ch]);
        }
        if (settings.chBaseline.contains(ch)){
            ui->tableWidgetSetup->item(ch+1, COL_BASELINE)->setText(QString::number(settings.chBaseline[ch]));
        }
    }
    updateGlobalCheckBox(COL_EN);
    updateGlobalCheckBox(COL_THRESHOLD_EN);
    updateCellEnabledState();
    tableWidgetChangeEnable = bak;
}

void ChannelsSetupForm::updateCellEnabledState()
{
    if(!config->device_configs.contains(selectedDevice))
        return;
    const DominoSettings &settings = config->device_configs[selectedDevice];
    const bool bak = tableWidgetChangeEnable;
    tableWidgetChangeEnable = false;
    const bool &globThrEnabled = settings.trigsetup.bit.threshold_en;
    for(int row=0; row<ui->tableWidgetSetup->rowCount(); ++row){
        const bool rowEnabled = ui->tableWidgetSetup->item(row, COL_EN)->checkState() != Qt::Unchecked;
        QTableWidgetItem *thrEnItem = ui->tableWidgetSetup->item(row, COL_THRESHOLD_EN);
        Q_ASSERT(thrEnItem!=nullptr);
        if(thrEnItem==nullptr)
            continue;
        Qt::ItemFlags flags = Qt::ItemIsSelectable;
        if(rowEnabled && globThrEnabled)
            flags |= Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
        thrEnItem->setFlags(flags);
        ui->tableWidgetSetup->cellWidget(row, COL_TRIG_THR)->
                setEnabled(globThrEnabled && rowEnabled && thrEnItem->checkState() != Qt::Unchecked);
        ui->tableWidgetSetup->cellWidget(row, COL_ZS_THR)->setEnabled(settings.zs_en);
    }
    tableWidgetChangeEnable = bak;
}

void ChannelsSetupForm::updateGlobalCheckBox(int col)
{
    bool hasOn=false;
    bool hasOff=false;
    for(int row=1;row<ui->tableWidgetSetup->rowCount(); ++row){
        if(ui->tableWidgetSetup->item(row, col)->checkState() == Qt::Checked)
            hasOn = true;
        else
            hasOff = true;
    }
    Qt::CheckState chSt;
    if(hasOff){
        if(hasOn)
            chSt = Qt::PartiallyChecked;
        else
            chSt = Qt::Unchecked;
    } else {
        chSt = Qt::Checked;
    }

    bool bak = tableWidgetChangeEnable;
    tableWidgetChangeEnable = false;
    ui->tableWidgetSetup->item(0, col)->setCheckState(chSt);
    tableWidgetChangeEnable = bak;
}

void ChannelsSetupForm::onTableWidgetChanged(int row, int column)
{
    if(!config->device_configs.contains(selectedDevice))
        return;
    DominoSettings &settings = config->device_configs[selectedDevice];

    if (!tableWidgetChangeEnable) return;
    bool changed = false;
    switch (column) {
    case COL_EN: {
        if(row==0) {
            switchEnabledAll();
        } else {
            bool en = (ui->tableWidgetSetup->item(row, COL_EN)->checkState() == Qt::Checked); // -1 for set/clear button
            if(settings.chEn.contains(row-1) && en != settings.chEn[row-1]){
                settings.chEn[row-1] = en;
                changed = true;
            }
        }
        break;
    }
    case COL_THRESHOLD_EN: {
        if(row==0) {
            switchThresholdAll();
        } else {
            bool en = (ui->tableWidgetSetup->item(row, COL_THRESHOLD_EN)->checkState() == Qt::Checked); // -1 for set/clear button
            if(settings.chTrigEn.contains(row-1) && en != settings.chTrigEn[row-1]){
                settings.chTrigEn[row-1] = en;
                changed = true;
                ui->tableWidgetSetup->cellWidget(row, COL_TRIG_THR)->setEnabled(
                            settings.trigsetup.bit.threshold_en && en);
            }
        }
        break;
    }
    default:
        return;
    }

    updateGlobalCheckBox(column);
    updateCellEnabledState();
    if (changed) Q_EMIT channelsSetupChanged();
}

void ChannelsSetupForm::onThresholdChanged(int value)
{
    if(!config->device_configs.contains(selectedDevice))
        return;
    DominoSettings &settings = config->device_configs[selectedDevice];

    if (!tableWidgetChangeEnable) return;
    for(int row=0; row<ui->tableWidgetSetup->rowCount(); ++row) {
        if(ui->tableWidgetSetup->cellWidget(row, COL_TRIG_THR) == sender()){
            if(row==0) {
                foreach (int ch, settings.chTrigThr.keys()) {
                    settings.chTrigThr[ch] = value;
                }
                restoreFormSettings();
            } else {
                settings.chTrigThr[row-1] = value;
            }
        } else if(ui->tableWidgetSetup->cellWidget(row, COL_ZS_THR) == sender()){
            if(row==0) {
                foreach (int ch, settings.chTrigThr.keys()) {
                    settings.chZsThr[ch] = value;
                }
                restoreFormSettings();
            } else {
                settings.chZsThr[row-1] = value;
            }
        } else
            continue;

        Q_EMIT channelsSetupChanged();
        return;
    }
}

void ChannelsSetupForm::switchEnabledAll()
{
    if(!config->device_configs.contains(selectedDevice))
        return;
    DominoSettings &settings = config->device_configs[selectedDevice];

    bool all_checked = true;
    Q_FOREACH(int i, settings.chEn.keys())
        if (i < settings.nch && !settings.chEn[i]) all_checked = false;
    settings.set_en_all(!all_checked);
    restoreFormSettings();
    Q_EMIT channelsSetupChanged();
}

void ChannelsSetupForm::switchThresholdAll()
{
    if(!config->device_configs.contains(selectedDevice))
        return;
    DominoSettings &settings = config->device_configs[selectedDevice];

    bool all_checked = true;
    foreach (bool checked, settings.chTrigEn.values()) {
        if(!checked){
            all_checked = false;
            break;
        }
    }
    foreach (int ch, settings.chTrigEn.keys()) {
        settings.chTrigEn[ch] = !all_checked;
    }
    restoreFormSettings();
    Q_EMIT channelsSetupChanged();
}
