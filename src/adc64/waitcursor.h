//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef WAITCURSOR_H
#define WAITCURSOR_H

#include <QCursor>
#include <QObject>

class QWidget;

class LocalWaitCursor: public QObject
{
public:
    Q_DISABLE_COPY(LocalWaitCursor)
    explicit LocalWaitCursor(QWidget *w);
    ~LocalWaitCursor() override;
private:
    QWidget * const widget;
    QCursor save_cursor;
};

class GlobalWaitCursor: public QObject
{
public:
    Q_DISABLE_COPY(GlobalWaitCursor)
    GlobalWaitCursor();
    ~GlobalWaitCursor() override;
private:
    QCursor save_cursor;
};

#endif // WAITCURSOR_H
