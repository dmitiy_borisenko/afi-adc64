//
//    Copyright 2011 Alexey Shutov
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef ChannelsSetupForm_H
#define ChannelsSetupForm_H

#include <QDialog>
#include "mldiscover/DeviceIndex.h"
#include "Config.h"

namespace Ui {
    class ChannelsSetupForm;
}

class ChannelsSetupForm : public QDialog
{
    Q_OBJECT

public:
    explicit ChannelsSetupForm(Config *s, QWidget *parent = nullptr);
    ~ChannelsSetupForm() override;

Q_SIGNALS:
    void channelsSetupChanged();

private Q_SLOTS:
    void setNumberOfChannels(DeviceIndex index, int num);
    void onTableWidgetChanged(int row, int column);
    void onThresholdChanged(int value);
    void onSetSettings();
    void onDeviceChange(const DeviceIndex &);
    void switchEnabledAll();
    void switchThresholdAll();

protected:
    void changeEvent(QEvent *e) override;

private:
    void restoreFormSettings();
    void updateCellEnabledState();
    void updateGlobalCheckBox(int col);

    Ui::ChannelsSetupForm * const ui;
    Config * const config;
    DeviceIndex selectedDevice;
    bool tableWidgetChangeEnable;
};

#endif // ChannelsSetupForm_H
