//
//    Copyright 2011 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef DOMINODEVICEMANAGER_H
#define DOMINODEVICEMANAGER_H

#include <QList>
#include <QMap>
#include <QObject>
#include <QSet>
#include <QSharedPointer>
#include <QVariant>

#include "adc64-common/AdcStatus.h"
#include "DeviceEntry.h"
#include "mldiscover/DeviceIndex.h"

class DominoDevice;
class QHostAddress;
class DominoSettings;
class EventData;
class DominoData;
class DeviceDescription;
class QTimer;

class DeviceDatabase : public QMap<QString, DeviceEntry> {};

class DominoDeviceManager : public QObject
{
    Q_OBJECT
public:
    explicit DominoDeviceManager(QObject *parent = nullptr);
    ~DominoDeviceManager() override;
//    DeviceProperties getProperties() const;
    bool isAllConnected() const;
//    QtMregDevice::DeviceId getDeviceId() const;
    QString getDeviceIdStr() const;
    QSet<int> getSupportedDevices() const { return supportedDevices; }

signals:
    void setupChanged(const DominoSettings &);
    void deserializeDelayChanged(int clockDelay, int dataDelay);
    void deviceConnected(const DeviceIndex &i, const QString &fw);
    void deviceDisconnected(const DeviceIndex &i);
//    void devicePropertiesChanged(const DeviceProperties &);
    void statusUpdated(const DeviceIndex &, const AdcStatus &);

    void tempChanged(DeviceIndex, double);
    void deviceInfoUpdated(DeviceIndex,int,int,bool);
    void dataAcquired(const EventData &);
    void monitorEventChanged(const EventData &);
    void clearEventNumber();
    void deviceUpdated(const DeviceDescription &dd);
    void deleteDevice(const DeviceIndex &);
    void deviceStatusUpdated(const DeviceIndex &, const QString &);
    void adcLockStatusUpdated(DeviceIndex, bool ok, quint8 locked);
    void adcRampStatusUpdated(DeviceIndex, bool ok, quint8 rampError);
    void startAdcClockDelay();

public slots:
    void deviceListUpdated(const QVector<DeviceDescription> &deviceList);
    void updateDeviceStatus(const QString &st);
//    void clearEventNumber();

private slots:
    void onDeviceConnect();
    void onDeviceDisconnect();
    void onDeviceTempChanged(double);
    void onDeviceDataAcquired(const DominoData &dominoData);
    void clearFullEventNumber();
    void eventTimeout();
    void findAdcClockDelay();
    void adcClkDelayUpdated(const DeviceIndex &index,const AdcClkStatus &s);

private:
    void removeAllDevices();
    void maybeSendMonitorEvent(const EventData &eventData);

    DeviceDatabase devices;
    QMap<DeviceIndex, QThread *> devThreads;
    QSet<int> supportedDevices;
//    DeviceProperties * const properties;
    EventData * const eventData;
    EventData * const monitorEvent;
    QTimer * const eventTimeoutTimer;
    QMap<DeviceIndex, AdcClkStatus> delayStatusMap;
};

#endif // DOMINODEVICEMANAGER_H
