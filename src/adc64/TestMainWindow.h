//
//    Copyright 2011 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef TESTMAINWINDOW_H
#define TESTMAINWINDOW_H

#include "mainwindow.h"

class TestMainWindow : public MainWindow
{
public:
    TestMainWindow();

private slots:
    void on_spinBoxch48Threshold_valueChanged(int );
    void on_pushButtonADCBITCLKDETECT_clicked();
    void on_pushButtonSTEST1_clicked();
    void on_pushButtonSTEST_clicked();
    void on_pushButtonADCSPITest_clicked();
    void on_pushButtonBitclkDelay_clicked();
    void on_pushButton_add_bitclk_delay_clicked();
    void on_pushButtonGTPDualTest_clicked();
    void on_pushButton_rm_clicked();
    void on_pushButton_t2_clicked();
    void on_pushButton_t1_clicked();
    void on_pushButtonTest2_clicked();
    void on_spinBoxADelay_valueChanged(int );
    void on_spinBoxWriteCNT_valueChanged(int );

};

#endif // TESTMAINWINDOW_H
