#ifndef READOUTWINDOWDIALOG_H
#define READOUTWINDOWDIALOG_H

#include <QDialog>

namespace Ui {
class ReadoutWindowDialog;
}

class ReadoutWindowDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ReadoutWindowDialog(int readCount, int afterTrig, QWidget *parent = nullptr);
    ~ReadoutWindowDialog() override;
    int getReadCount() const;
    int getTrigDelay() const;

private slots:
    void on_spinBoxLeftEdge_valueChanged(int val);
    void on_spinBoxRightEdge_valueChanged(int val);

private:
    void updateLimits();
    void updateLabels();
    void info();

    Ui::ReadoutWindowDialog * const ui;
    const int readCount;
    const int afterTrig;
    int leftEdge;
    int rightEdge;
};

#endif // READOUTWINDOWDIALOG_H
