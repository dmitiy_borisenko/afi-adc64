//
//    Copyright 2011 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "DominoDeviceManager.h"

#include <QDebug>
#include <QList>
#include <QMap>
#include <QMetaType>
#include <QStringList>
#include <QThread>
#include <QTimer>

#include "Config.h"
#include "DeviceEntry.h"
#include "DeviceProperties.h"
#include "adc64-common/DominoDeviceFactory.h"
#include "EventData.h"
#include "adc64-common/dominodevice.h"
#include "mldiscover/DeviceDescription.h"
#include "base/threadQuit.h"
#include "util/QDateTimeCompat.h"

namespace {
const int monitorEventTimeMs = 300;
const int eventTimeoutMs = 500;
}

DominoDeviceManager::DominoDeviceManager(QObject *parent) :
    QObject(parent),
    eventData(new EventData),
    monitorEvent(new EventData),
//    properties(new DeviceProperties())
    eventTimeoutTimer(new QTimer(this))
{
    qRegisterMetaType<EventData>();
    qRegisterMetaType<AdcClkStatus>();
    supportedDevices
            << DEVICE_ID_TQDC16VS_DIG
            << DEVICE_ID_ADC64
            << DEVICE_ID_ADC64S2
            << DEVICE_ID_ADC64WR
            << DEVICE_ID_ADC64VE
            << DEVICE_ID_ADC64ECAL
            << DEVICE_ID_ADC64VE_XGE
            << DEVICE_ID_ADC8E
//            << DEVICE_ID_AnyDevice
            << DEVICE_ID_ADCM163;
    eventTimeoutTimer->setSingleShot(true);
    eventTimeoutTimer->setInterval(eventTimeoutMs);
    connect(eventTimeoutTimer, SIGNAL(timeout()), SLOT(eventTimeout()));
    qRegisterMetaType<AdcStatus>();
}

DominoDeviceManager::~DominoDeviceManager()
{
    removeAllDevices();
    qDebug()<<"DominoDeviceManager destroyed";
}

void DominoDeviceManager::deviceListUpdated(const QVector<DeviceDescription> &deviceList)
{
    removeAllDevices();
    Q_FOREACH(const DeviceDescription dd, deviceList)
    {
        DominoDevice * const dev = DominoDeviceFactory::create(dd);
        QThread * thread;

        if(!devThreads.contains(dd.getIndex()))
            devThreads[dd.getIndex()] = new QThread(this);
        thread = devThreads[dd.getIndex()];

        QString key = QString("%1;%2")
                .arg(dd.device_id, 2, 16, QChar('0'))
                .arg(dd.serial_id, 8, 16, QChar('0'));
        DeviceEntry ent;
        ent.dev = DominoDevicePtr(dev);
        devices.insert(key, ent);
        connect(this, SIGNAL(setupChanged(DominoSettings)), dev, SLOT(on_setupChanged(DominoSettings)));
        connect(this, SIGNAL(deserializeDelayChanged(int,int)), dev, SLOT(on_deserializeDelayChanged(int,int)));
        connect(this, SIGNAL(clearEventNumber()), dev, SLOT(clearEventNumber()));
        connect(dev, SIGNAL(deviceConnected()), this, SLOT(onDeviceConnect()));
        connect(dev, SIGNAL(deviceDisconnected()), this, SLOT(onDeviceDisconnect()));
        connect(dev, SIGNAL(TempChanged(double)), SLOT(onDeviceTempChanged(double)));
        connect(dev, SIGNAL(statusUpdated(DeviceIndex,AdcStatus)), SIGNAL(statusUpdated(DeviceIndex, AdcStatus)));
        connect(dev, SIGNAL(deviceInfoUpdated(DeviceIndex,int,int, bool)), SIGNAL(deviceInfoUpdated(DeviceIndex,int,int, bool)));
        connect(dev, SIGNAL(dataAcquired(DominoData)), SLOT(onDeviceDataAcquired(DominoData)));
        connect(this, SIGNAL(deviceUpdated(DeviceDescription)),
                dev, SLOT(deviceUpdated(DeviceDescription)));
        connect(this, SIGNAL(deleteDevice(DeviceIndex)), dev, SLOT(deleteHardware(DeviceIndex)));
        connect(dev, SIGNAL(destroyed(QObject*)), thread, SLOT(quit()));
        connect(dev, SIGNAL(setStatus(QString)), SLOT(updateDeviceStatus(QString)));
        connect(dev, SIGNAL(adcLockStatusUpdated(DeviceIndex,bool,quint8)),
                SIGNAL(adcLockStatusUpdated(DeviceIndex,bool,quint8)));
        connect(dev, SIGNAL(adcRampStatusUpdated(DeviceIndex,bool,quint8)),
                SIGNAL(adcRampStatusUpdated(DeviceIndex,bool,quint8)));
        connect(this, SIGNAL(startAdcClockDelay()), dev, SLOT(FindAdcClkDelay()));
        connect(dev, SIGNAL(adcClkDelayUpdated(DeviceIndex,AdcClkStatus)), SLOT(adcClkDelayUpdated(DeviceIndex,AdcClkStatus)));

        dev->moveToThread(thread);
        thread->setObjectName(dd.getSerialIdStr());
        thread->start();

        if(dd.ip_addr)
            emit deviceUpdated(dd);
    }
    //    qDebug() << "DominoDeviceManager::setHostAddressList:" << devices.size() << "devices created";
}

void DominoDeviceManager::updateDeviceStatus(const QString &st)
{
    auto *dev = qobject_cast<DominoDevice *>(sender());
    if (dev == nullptr)
        return;
    emit deviceStatusUpdated(dev->getDeviceIndex(), st);
}

//void DominoDeviceManager::clearEventNumber()
//{
//    Q_FOREACH(DeviceEntry ent, devices)
//        QMetaObject::invokeMethod(ent.dev.data(), "clearEventNumber");
//}

bool DominoDeviceManager::isAllConnected() const
{
    if (devices.isEmpty())
        return false;
    Q_FOREACH(DeviceEntry ent, devices)
        if (!ent.dev->isConnected())
            return false;
    return true;
}

//QtMregDevice::DeviceId DominoDeviceManager::getDeviceId() const
//{
//}

QString DominoDeviceManager::getDeviceIdStr() const
{
    QStringList sl;
    Q_FOREACH(DeviceEntry ent, devices)
        sl.push_back(ent.dev->getDeviceIdStr());
    return sl.join("\n");
}

void DominoDeviceManager::onDeviceConnect()
{
    const DominoDevice *dev = qobject_cast<DominoDevice *>(sender());
    if (dev) {
//        qDebug() << "DominoDeviceManager: devices connected "
//                 << dev->getDeviceIndex().getSerialStr();
        emit deviceConnected(dev->getDeviceIndex(), dev->getFirmwareVersionStr());
    }
}

void DominoDeviceManager::onDeviceDisconnect()
{
    const DominoDevice *dev = qobject_cast<DominoDevice *>(sender());
    if (dev) {
        qDebug() << "DominoDeviceManager: device disconnected "
                 << dev->getDeviceIndex().getSerialStr();
        emit deviceDisconnected(dev->getDeviceIndex());
    }
}

void DominoDeviceManager::clearFullEventNumber()
{
    eventData->clearFullEventNumber();
    emit clearEventNumber();
}

void DominoDeviceManager::eventTimeout()
{
    if(eventData->timestamp.elapsed() > eventTimeoutMs)
        qWarning() << QString("EventData timed out, number of event blocks mismatch: %3 blocks, %4 devices")
                      .arg(eventData->size()).arg(devices.size());
}

void DominoDeviceManager::findAdcClockDelay()
{
    delayStatusMap.clear();
    emit startAdcClockDelay();
}

void DominoDeviceManager::adcClkDelayUpdated(const DeviceIndex &index, const AdcClkStatus &s)
{
    delayStatusMap[index] = s;
    qInfo() << "Got status from:"<< index.getIdent()<< " "<< delayStatusMap.size() << " of "<< devices.size();
    // check if all devices present in map
    if (devices.size() != delayStatusMap.size())
        return;
    AdcClkStatus totalStatus = s;
    for (auto i : delayStatusMap) {
        totalStatus &= i;
    }
    for (DeviceIndex i : delayStatusMap.keys()) {
        const AdcClkStatus &devStatus = delayStatusMap[i];
        qInfo().noquote() << "";
        qInfo().noquote() << QString("=== %1 (f/w = %2; temp = %3) ===")
                                 .arg(i.getIdent())
                                 .arg(devStatus.fwStr)
                                 .arg(devStatus.temp, 0, 'f', 1);
        qInfo().noquote() << devStatus.toString();
    }
    bool multi = delayStatusMap.size() > 1;
    if (multi) {
        qInfo().noquote() << "";
        qInfo().noquote() << QString("=== %1 ===").arg("Aggregated status");
        qInfo().noquote() << totalStatus.toString();
    }
}

void DominoDeviceManager::removeAllDevices()
{
    emit deleteDevice(DeviceIndex());
    QElapsedTimer quitTimer;
    quitTimer.start();
    foreach (const DeviceIndex &i, devThreads.keys()) {
        if(!threadQuiting(devThreads[i], 100, &quitTimer)) {
            qWarning() << QString("Failed to stop device thread %1").arg(i.getIdent());
            devThreads[i]->terminate();
        }
    }
    devices.clear();
}

void DominoDeviceManager::onDeviceTempChanged(double value)
{
    auto *dev = qobject_cast<DominoDevice *>(sender());
    if(dev)
        emit tempChanged(dev->getDeviceIndex(), value);
}

void DominoDeviceManager::maybeSendMonitorEvent(const EventData &eventData)
{
    const qint64 dt = monitorEvent->timestamp.msecsTo(eventData.timestamp);
    if (!monitorEvent->timestamp.isValid() || dt > monitorEventTimeMs || dt < 0) {
        *monitorEvent = eventData;
        emit monitorEventChanged(*monitorEvent);
    }
}

void DominoDeviceManager::onDeviceDataAcquired(const DominoData &dominoData)
{
    quint64 key = dominoData.serial_id;

//    if (eventData->find(key) != eventData->end()) {
//    } else {
//    }
    if (eventData->size() > devices.size()) {
        qWarning() << "Event demultiplexer: Too much different id numbers, event buffer reset";
        eventData->clear();
    }
    if(eventData->isEmpty()) {
        eventTimeoutTimer->start();
        eventData->timestamp.start();
    }
    (*eventData)[key] = dominoData;

//    bool match = eventData->timestampTrigMatch();
//    // emit data only when all event numbers match
//    if (!match) {
//        qWarning() << "Timestamp mismatch";
//        return;
//    }

    if (eventData->size() == devices.size()) {
        eventTimeoutTimer->stop();
        eventData->FullEventNumber++;
        maybeSendMonitorEvent(*eventData);
        emit dataAcquired(*eventData);
        eventData->clear();
        //        qDebug() << "Event number Ok";
    }
}
