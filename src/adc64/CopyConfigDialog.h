#ifndef COPYCONFIGDIALOG_H
#define COPYCONFIGDIALOG_H

#include <QDialog>
#include "Config.h"
#include "mldiscover/DeviceIndex.h"

namespace Ui {
class CopyConfigDialog;
}

class CopyConfigDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CopyConfigDialog(Config *config, QWidget *parent = nullptr);
    ~CopyConfigDialog() override;

private slots:
    void radioButtonToggled(bool checked);
    void checkBoxToggled(bool checked);
    void on_pushButtonCopy_clicked();

private:
    DeviceIndex get_device_index_by_serial(const QString& serial) const;

    Ui::CopyConfigDialog * const ui;
    Config * const config;

    QList<DeviceIndex> all_keys;
    DeviceIndex from_key;
    QList<DeviceIndex> to_keys;
};

#endif // COPYCONFIGDIALOG_H
