#include "SparseDialog.h"

#include "Config.h"
#include "adc64-common/dominosettings.h"
#include "ui_SparseDialog.h"

SparseDialog::SparseDialog(Config *config, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SparseDialog),
    config(config)
{
    ui->setupUi(this);
    block_signal_widgets_list.append(ui->radioButtonOpt1);
    block_signal_widgets_list.append(ui->radioButtonOpt2);
    block_signal_widgets_list.append(ui->radioButtonOpt3);
    block_signal_widgets_list.append(ui->radioButtonOpt4);
    block_signal_widgets_list.append(ui->checkBoxEnable);
    block_signal_widgets_list.append(ui->spinBoxReadCellNum);
    block_signal_widgets_list.append(ui->spinBoxOffset);
    block_signal_widgets_list.append(ui->spinBoxPeriod);
}

SparseDialog::~SparseDialog()
{
    delete ui;
}

void SparseDialog::setOffsetMaximum(const DeviceIndex &index, quint16 max)
{
    devMaxSampleCnt[index] = max;
    if(selectedDevice == index){
        ui->spinBoxReadCellNum->setMaximum(max);
        ui->spinBoxOffset->setMaximum(max);
    }
}

void SparseDialog::blockOffset(const DeviceIndex &index, bool blockOffset)
{
    if(blockOffset)
        devBlockOffset.insert(index);
    else
        devBlockOffset.remove(index);
    updateEnabling();
}

void SparseDialog::onDeviceChange(const DeviceIndex &di)
{
    selectedDevice = di;
    restoreFormSettings();

    if(devMaxSampleCnt.contains(selectedDevice)) {
        ui->spinBoxReadCellNum->setMaximum(devMaxSampleCnt[selectedDevice]);
        ui->spinBoxOffset->setMaximum(devMaxSampleCnt[selectedDevice]);
    } else {
        if(config->device_configs.contains(selectedDevice)) {
            const SparseParams &params = config->device_configs[selectedDevice].sparseParams;
            ui->spinBoxReadCellNum->setMaximum(params.readCellNum);
            ui->spinBoxOffset->setMaximum(params.offset);
        }
    }
    setWindowTitle(QString("Sparse parameters (%1)").arg(di.getSerialStr()));
}

void SparseDialog::restoreFormSettings()
{
    if(!config->device_configs.contains(selectedDevice)){
        updatesEnabled();
        return;
    }

    const SparseParams &params = config->device_configs[selectedDevice].sparseParams;
    block_gui_signals(true);

    ui->checkBoxEnable->setChecked(params.en);
    if(ui->spinBoxReadCellNum->maximum() < params.readCellNum)
        ui->spinBoxReadCellNum->setMaximum(params.readCellNum);
    ui->spinBoxReadCellNum->setValue(params.readCellNum);
    if(ui->spinBoxOffset->maximum() < params.offset)
        ui->spinBoxOffset->setMaximum(params.offset);
    ui->spinBoxOffset->setValue(params.offset);
    ui->spinBoxPeriod->setValue(params.period);
    switch (params.pointNumber) {
    case 0:
        ui->radioButtonOpt1->setChecked(true);
        break;
    case 1:
        ui->radioButtonOpt2->setChecked(true);
        break;
    case 2:
        ui->radioButtonOpt3->setChecked(true);
        break;
    case 3:
        ui->radioButtonOpt4->setChecked(true);
        break;
    default:
        break;
    }
    block_gui_signals(false);
    updateEnabling();
}

void SparseDialog::updateEnabling()
{
    bool enabled = config->device_configs.contains(selectedDevice);
    ui->checkBoxEnable->setEnabled(enabled);
    if(enabled) {
        const SparseParams &params = config->device_configs[selectedDevice].sparseParams;
        enabled = params.en;
    }
    ui->spinBoxReadCellNum->setEnabled(enabled);
    ui->spinBoxOffset->setEnabled(enabled && !devBlockOffset.contains(selectedDevice));
    ui->spinBoxPeriod->setEnabled(enabled);
    ui->groupBox->setEnabled(enabled);
}

void SparseDialog::block_gui_signals(bool block)
{
    for(int i=0; i<block_signal_widgets_list.size(); ++i){
        block_signal_widgets_list[i]->blockSignals(block);
    }
}

void SparseDialog::on_checkBoxEnable_toggled(bool checked)
{
    if(!config->device_configs.contains(selectedDevice))
        return;
    config->device_configs[selectedDevice].sparseParams.en = checked;
    emit setupChanged();
    updateEnabling();
}

void SparseDialog::on_spinBoxReadCellNum_valueChanged(int arg1)
{
    if(!config->device_configs.contains(selectedDevice))
        return;
    config->device_configs[selectedDevice].sparseParams.readCellNum = arg1;
    emit setupChanged();
}

void SparseDialog::on_spinBoxOffset_valueChanged(int arg1)
{
    if(!config->device_configs.contains(selectedDevice))
        return;
    config->device_configs[selectedDevice].sparseParams.offset = arg1;
    emit setupChanged();
}

void SparseDialog::on_spinBoxPeriod_valueChanged(int arg1)
{
    if(!config->device_configs.contains(selectedDevice))
        return;
    config->device_configs[selectedDevice].sparseParams.period = arg1;
    emit setupChanged();
}

void SparseDialog::on_radioButtonOpt1_toggled(bool checked)
{
    if(checked){
        config->device_configs[selectedDevice].sparseParams.pointNumber = 0;
        emit setupChanged();
    }
}

void SparseDialog::on_radioButtonOpt2_toggled(bool checked)

{
    if(checked){
        config->device_configs[selectedDevice].sparseParams.pointNumber = 1;
        emit setupChanged();
    }
}

void SparseDialog::on_radioButtonOpt3_toggled(bool checked)
{
    if(checked){
        config->device_configs[selectedDevice].sparseParams.pointNumber = 2;
        emit setupChanged();
    }
}

void SparseDialog::on_radioButtonOpt4_toggled(bool checked)
{
    if(checked){
        config->device_configs[selectedDevice].sparseParams.pointNumber = 3;
        emit setupChanged();
    }
}
