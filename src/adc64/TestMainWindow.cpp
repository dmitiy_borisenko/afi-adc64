//
//    Copyright 2011 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "TestMainWindow.h"

TestMainWindow::TestMainWindow()
{
}

void MainWindow::on_pushButtonGTPDualTest_clicked()
{

}

void MainWindow::on_pushButton_add_bitclk_delay_clicked()
{
    int val = ui->spinBox_bitclk_delay->value();
    qDebug() << "adding " << val << "to MAX550 DAC";
    dominoDevice->DacMax550Write(0x0D, val&0xFF);
}

void MainWindow::on_pushButtonBitclkDelay_clicked()
{
    int val = ui->spinBoxBitclkDelay->value();
    qDebug() << "adding " << val << "78ps tap(s)";
    dominoDevice->IncreaseBitclkDelay(val);
}

void MainWindow::on_pushButtonADCSPITest_clicked()
{
    bool testok = dominoDevice->SimpleAdcTest();
    qDebug()<< "Test " << (testok ? "passed" : "failed");
}

void MainWindow::on_pushButtonSTEST_clicked()
{
    dominoDevice->SimpleAdcSpiOperation();
}

void MainWindow::on_pushButtonSTEST1_clicked()
{
    dominoDevice->SimpleAdcSpiOperation1();
}

void MainWindow::on_pushButtonADCBITCLKDETECT_clicked()
{
    bool testok;
    std::vector<bool> test;
    test.clear();
    ui->pushButtonADCBITCLKDETECT->setText("Testing ...");
    for (int i=0; i<64; i++)
    {
        testok = dominoDevice->SimpleAdcTest();
        test.push_back(testok);
        dominoDevice->IncreaseBitclkDelay(1);
    }
    ui->pushButtonADCBITCLKDETECT->setText("ADC BITCLK DETECT");

    for (size_t i=0; i<test.size(); i++)
        qDebug()<< "Test for delay = " << i << (test[i] ? "passed" : "failed");
}

void MainWindow::on_pushButton_t1_clicked()
{
//    dominoDevice->regWrite( ui->spinBox_reg->value() , ui->spinBox_data->value() );
}

void MainWindow::on_pushButton_t2_clicked()
{
//    qDebug() << "reg " << ui->spinBox_reg->value() << "=" << hex << dominoDevice->regRead( ui->spinBox_reg->value());
}

void MainWindow::on_pushButton_rm_clicked()
{
    LocalWaitCursor wc(this);


    std::vector<quint32> rd;
    dominoDevice->SimpleAdcSpiOperation1();
    //  rd = dominoDevice->memReadBlk(0,10);
    //  for (int i=0; i<10; i++)
    //    qDebug() << "data " << i << hex << rd[i];
    //    DrawScope();
    //    qDebug() << "end of memory read";
}

void MainWindow::on_pushButtonTest2_clicked()
{
//    ui->groupBoxWRReg->show();
//    ui->groupBoxTrigger->show();
}

void MainWindow::on_spinBoxWriteCNT_valueChanged(int newval)
{
    setup.writecellnumber=newval;
    setupDevice();
}

void MainWindow::on_spinBoxADelay_valueChanged(int newval)
{
    setup.analogDelay = newval;
    setupDevice();
}

