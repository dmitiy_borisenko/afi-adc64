#ifndef ADC64STARTUPOPTIONS_H
#define ADC64STARTUPOPTIONS_H

#include "mongo/StartupOptions.h"

#include <QCommandLineOption>
#include <QMap>
#include <QString>
#include <QStringList>

class Adc64StartupOptions  : public StartupOptions
{
public:
    Adc64StartupOptions(const QString &program_type_);
    void parse_options();

    bool pcb_ch = false;
private:
    void show_config_list();

    QCommandLineOption pcb_ch_option;
};

#endif // ADC64STARTUPOPTIONS_H
