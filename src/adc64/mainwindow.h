#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QElapsedTimer>
#include <QMainWindow>

#include "Adc64StartupOptions.h"
#include "Config.h"
#include "PreamplifierDialog.h"
#include "channelssetupform.h"
#include "adc64-common/dominodata.h"
#include "configuration-manager/ConfigurationManager.h"

namespace Ui {
    class MainWindow;
}

class AdcStatus;
class DeviceDescription;
class DiscoverDialog;
class DominoData;
class DominoDeviceManager;
class EventData;
class EventWriter;
class QDialog;
class QLabel;
class QThread;
class QwtPlotMarker;
class SparseDialog;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(const Adc64StartupOptions &startup_options, QWidget *parent = nullptr);
    ~MainWindow() override;
    void enabledPcbSwitching();

Q_SIGNALS:
    void setupChanged(const DominoSettings &);
    void updateChannelsSetupFormSettings();
    void deviceListUpdated(const QVector<DeviceDescription> &);
    void selectedDeviceChanged(const DeviceIndex &);
    void destroyAll();
    void clearEventNumber();
    void deserializeDelayChanged(int clockDelay, int dataDelay);

protected:
    void changeEvent(QEvent *e) override;
    void closeEvent(QCloseEvent *event) override;

private Q_SLOTS:
    //--Menu bar slots
    //quit
    //options
    void on_actionChange_device_triggered();
    void on_actionCalibrate_triggered();
    void on_actionSet_read_out_window_triggered();
    void on_actionSparse_parameters_triggered();
    void on_actionPreamplifier_triggered();
    void on_actionCopyConfig_triggered();
    void on_actionShow_PCB_ch_toggled(bool val);
    void on_actionAdd_FIR_preset_triggered();
    void on_actionRemove_all_presets_triggered();
    void on_actionConfiguration_manager_triggered();
    //about
    void on_actionDocs_triggered();
    void on_actionChannel_map_triggered();
    void on_actionCheckForUpdates_triggered();
    void on_actionAbout_triggered();

    // Other slots
    void channelsSetupFormChanged();
    void onDeviceConnect(const DeviceIndex &i, const QString &fw);
    void onDeviceDisconnect(const DeviceIndex &i);
    void updateAdcStatus(const DeviceIndex &index, const AdcStatus &status);
    void deviceStatusUpdated(const DeviceIndex &index, const QString &s) const;
    void adcLockStatusUpdated(const DeviceIndex &index, bool ok, quint8 locked) const;
    void adcRampStatusUpdated(const DeviceIndex &index, bool ok, quint8 rampError) const;
    void deviceInfoUpdated(DeviceIndex index, int chNum, int maxSampleCnt, bool blockOffset);
    void DrawScope(const EventData &, bool force = false);
    void writerStatusUpdated(const QString &st);
    void deviceUpdated(const DeviceDescription &dd);
    void updateSparseMarkers();
    void switch_to(const QString &program_index, const QString& configuration_name);

    //--GUI slots
    //--expert
    void on_spinBoxClockDelay_valueChanged(int clock_delay);
    void on_spinBoxDataDelay_valueChanged(int data_delay);
    void on_pushButtonSendDelay_clicked();
    void on_checkBoxInvertSignal_toggled(bool invert);

    //--readout window
    void on_spinBoxSize_valueChanged(int newval);
    void on_spinBoxLatency_valueChanged(int newval);

    //--trigger
    void on_checkBoxTrigTimer_toggled(bool checked);
    void on_checkBoxTrigThreshold_toggled(bool checked);
    void on_comboBoxThrTrig_currentIndexChanged(const QString &arg1);
    void on_checkBoxTrigLemo_toggled(bool checked);

    //--dsp roundoff
    void on_groupBoxDsp_toggled(bool checked);
    void on_spinBoxFirRoundoff_valueChanged(int value);
    //--dsp tail
    void on_groupBoxTailCancellation_toggled(bool checked);
    void on_comboBoxFirPreset_currentIndexChanged(int index);
    //--dsp maf
    void on_groupBoxMaf_toggled(bool checked);
    void on_comboBoxDspMafType_currentIndexChanged(int index);
    void on_comboBoxDspMafSel_activated(int index);
    void on_spinBoxDspBlcThr_valueChanged(int value);

    //--zero
    void on_groupBoxZS_toggled(bool checked);
    void on_comboBoxZSThr_currentIndexChanged(const QString &arg1);
    void on_checkBoxSoftwareZS_toggled(bool checked);

    //--find device
    void on_pushButtonFindDevice_clicked();
    void on_tableWidgetDevices_itemSelectionChanged();

    //--start/stop
    void on_pushButtonStart_clicked();
    void on_pushButtonStop_clicked();
    void on_checkBoxW2D_toggled(bool checked);
    void on_pushButtonChannelsSetup_clicked();
    void on_comboBoxOctalADC_currentIndexChanged(int);
    void on_checkBoxSSM_toggled(bool checked);

private:
    void enableRunControlButtons();
    void setupChanged();
    void read_config();
    void block_gui_signals(bool block);
    void restoreFormValues();
    void updateThresholdTrigger(bool enabled);
    void addDeviceRow(const DeviceIndex &index);
    int getDeviceRow(const DeviceIndex &index) const;
    DeviceIndex getDeviceIndex(int row) const;
    void initTable() const;
    void updateTableSize() const;
    void updateDisplayedChannels();
    void updateAdcChGroups();
    void update_fir_combo_box();

    Ui::MainWindow * const ui;
    Config config;
    ConfigurationManager *configuration_manager = nullptr;
    QList<QWidget*> block_signal_widgets_list;
    QLabel * const writerLabel;
    QLabel * const evLabel;
    DominoDeviceManager * const deviceMgr;
    EventData * const lastEventData;
    QElapsedTimer scopeDisplayTime;
    EventWriter * const eventWriter;
    QThread * const deviceMngThread;
    QThread * const writerThread;
    ChannelsSetupForm * const channelsSetupForm;
    SparseDialog * const sparseDialog;
    PreamplifierDialog * preamplifier_dialog;
    DiscoverDialog * const discoverDialog;
    QDialog * channelMapDialog = nullptr;
    DeviceIndex selectedDevice;
    QMap<DeviceIndex, int> devMaxSampleCnt;
    int totalChNum = 0;
    QMap<int, bool> displayEn; // which channels we should draw
    QVector<QwtPlotMarker *> sparseMarkers;
    QMap<DeviceIndex, bool> devNegLat;
    bool pcbSwitchingEnabled;
    bool debug_mode;
    QMap<DeviceIndex, int> devChOffset;
};

#endif // MAINWINDOW_H
