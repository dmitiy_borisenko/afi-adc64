QT *= network

SOURCES += \
    $$PWD/Adc64StartupOptions.cpp \
    $$PWD/AddFirPresetDialog.cpp \
    $$PWD/CalibrationDialog.cpp \
    $$PWD/Config.cpp \
    $$PWD/CopyConfigDialog.cpp \
    $$PWD/DeviceEntry.cpp \
    $$PWD/DeviceProperties.cpp \
    $$PWD/DominoDeviceManager.cpp \
    $$PWD/EventData.cpp \
    $$PWD/EventWriter.cpp \
    $$PWD/PreamplifierDialog.cpp \
    $$PWD/ReadoutWindowDialog.cpp \
    $$PWD/SparseDialog.cpp \
    $$PWD/channelssetupform.cpp \
    $$PWD/mainwindow.cpp \
    $$PWD/main.cpp\
    $$PWD/waitcursor.cpp \


HEADERS += \
    $$PWD/Adc64StartupOptions.h \
    $$PWD/AddFirPresetDialog.h \
    $$PWD/CalibrationDialog.h \
    $$PWD/Config.h \
    $$PWD/CopyConfigDialog.h \
    $$PWD/DeviceEntry.h \
    $$PWD/DeviceProperties.h \
    $$PWD/DominoDeviceManager.h \
    $$PWD/EventData.h \
    $$PWD/EventWriter.h \
    $$PWD/PreamplifierDialog.h \
    $$PWD/ReadoutWindowDialog.h \
    $$PWD/SparseDialog.h \
    $$PWD/channelssetupform.h \
    $$PWD/mainwindow.h \
    $$PWD/waitcursor.h \


FORMS += \
    $$PWD/AddFirPresetDialog.ui \
    $$PWD/PreamplifierDialog.ui \
    $$PWD/mainwindow.ui \
    $$PWD/channelssetupform.ui \
    $$PWD/CalibrationDialog.ui \
    $$PWD/SparseDialog.ui \
    $$PWD/ReadoutWindowDialog.ui \
    $$PWD/CopyConfigDialog.ui \


RESOURCES += \
    $$PWD/adc64.qrc
