#include "PreamplifierDialog.h"
#include "ui_PreamplifierDialog.h"

PreamplifierDialog::PreamplifierDialog(Config *config, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PreamplifierDialog),
    config(config)
{
    ui->setupUi(this);

    block_signal_widgets_list.append(ui->groupBox);
    block_signal_widgets_list.append(ui->radioButtonVersion1);
    block_signal_widgets_list.append(ui->radioButtonVersion2);
    block_signal_widgets_list.append(ui->spinBoxBaseline);
    block_signal_widgets_list.append(ui->spinBoxLevel);

    QString gbox_style_1 = (
    "   QGroupBox::title{                   "
    "       background: white;              "
    "       border: 1px solid gray;         "
    "       border-radius: 3px;             "
    "       padding: 2px;                   "
    "   }                                   "
    "   QGroupBox{                          "
    "       background: rgb(216, 231, 235); "
    "       border: 1px solid gray;         "
    "       border-radius: 3px;             "
    "       margin-top: 6px;                "
    "   }                                   "
    );

    ui->groupBox->setStyleSheet(gbox_style_1);
    reset_gui();
}

PreamplifierDialog::~PreamplifierDialog()
{
    delete ui;
}

void PreamplifierDialog::reset_gui()
{
    block_gui_signals(true);
    setWindowTitle(QString("Preamplifier - %1").arg(selected_device.getSerialStr()));
    const DominoSettings &settings = config->device_configs[selected_device];

    ui->spinBoxBaseline->setValue(settings.ad5622op.data_baseline);
    ui->spinBoxLevel->setValue(settings.ad5622op.data_level);

    if (settings.amp_board_set == AMP_BOARD_AD5622) ui->radioButtonVersion1->setChecked(true);
    if (settings.amp_board_set == AMP_BOARD_AD5622v2) ui->radioButtonVersion2->setChecked(true);

    block_gui_signals(false);
}

void PreamplifierDialog::device_changed(DeviceIndex index)
{
    selected_device = index;
    reset_gui();
}

void PreamplifierDialog::on_groupBox_toggled(bool checked)
{
    if(!config->device_configs.contains(selected_device)) return;
    DominoSettings &settings = config->device_configs[selected_device];
    if(checked) {
        if (ui->radioButtonVersion1->isChecked()) settings.amp_board_set = AMP_BOARD_AD5622;
        if (ui->radioButtonVersion2->isChecked()) settings.amp_board_set = AMP_BOARD_AD5622v2;
    } else {
        settings.amp_board_set = AMP_BOARD_NONE;
    }
    emit config_changed();
}

void PreamplifierDialog::on_radioButtonVersion1_toggled(bool checked)
{
    if(!checked) return;
    if(!config->device_configs.contains(selected_device)) return;
    config->device_configs[selected_device].amp_board_set = AMP_BOARD_AD5622;
    emit config_changed();
}

void PreamplifierDialog::on_radioButtonVersion2_toggled(bool checked)
{
    if(!checked) return;
    if(!config->device_configs.contains(selected_device)) return;
    config->device_configs[selected_device].amp_board_set = AMP_BOARD_AD5622v2;
    emit config_changed();
}

void PreamplifierDialog::on_spinBoxBaseline_valueChanged(int baseline)
{
    if(!config->device_configs.contains(selected_device)) return;
    DominoSettings &settings = config->device_configs[selected_device];
    settings.ad5622op.data_baseline = baseline;
    emit config_changed();
}

void PreamplifierDialog::on_spinBoxLevel_valueChanged(int level)
{
    if(!config->device_configs.contains(selected_device)) return;
    DominoSettings &settings = config->device_configs[selected_device];
    settings.ad5622op.data_level = level;
    emit config_changed();
}

void PreamplifierDialog::block_gui_signals(bool block)
{
    for(int i=0; i<block_signal_widgets_list.size(); ++i){
        block_signal_widgets_list[i]->blockSignals(block);
    }
}
