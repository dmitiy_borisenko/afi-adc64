//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "waitcursor.h"

#include <QApplication>
#include <QCursor>
#include <QWidget>

LocalWaitCursor::LocalWaitCursor(QWidget *w)
    : widget(w)
{
    if (widget) {
        save_cursor = widget->cursor();
        widget->setCursor(Qt::WaitCursor);
    }
}

LocalWaitCursor::~LocalWaitCursor()
{
    if (widget)
        widget->setCursor(save_cursor);
}

GlobalWaitCursor::GlobalWaitCursor()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
}

GlobalWaitCursor::~GlobalWaitCursor()
{
    QApplication::restoreOverrideCursor();
}
