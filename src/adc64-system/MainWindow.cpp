//
//    Copyright 2014-2015 Ivan Filippov
//    Copyright 2014-2015 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "adc64-common/dominodevice.h"
#include "adc64-common/DominoDeviceFactory.h"
#include "device-discover/DiscoverDialog.h"
#include "remote-control-server/RemoteControlServer.h"
#include "remote-control-server/RunControlCommands.h"
#include "remote-control-server/EvNumChecker.h"
#include "base/threadQuit.h"
#include "WrDaqDialog.h"
#include "pnp-server/PNPServer.h"
#include "StopConditionsDialog.h"
#include "DeviceSettingsDialog.h"
#include "AdcDataBase.h"
#include "mongo/StartupOptions.h"
//#include "configuration-manager/ConfigurationManagerDialog.h"
#include "m-stream-output/StatisticOutput.h"
#include "base/AboutBox.h"
#include "base/ThreadController.h"

#include <cstdlib>
#include <QDateTime>
#include <QDialogButtonBox>
#include <QDebug>
#include <QElapsedTimer>
#include <QFileDialog>
#include <QFileInfo>
#include <QHostAddress>
#include <QHostInfo>
#include <QHBoxLayout>
#include <QInputDialog>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>
#include <QNetworkInterface>
#include <QScrollBar>
#include <QShortcut>
#include <QSignalMapper>
#include <QSpinBox>
#include <QStringList>
#include <QThread>
#include <QTimer>

namespace {
enum { COLUMN_DEV_ID_STR, COLUMN_SERIAL_STR, COLUMN_SLOT, COLUMN_IP, COLUMN_TEMP, COLUMN_EVENT,
       COLUMN_TRIG_ON_XOFF, COLUMN_SYNCH_STATUS, COLUMN_ADC_STATE_OK,
       COLUMN_WR_TIME, COLUMN_WR_TIME_OFFSET,
       COLUMN_WR_LINK_ERROR, COLUMN_WR_SYNC_LOST, COLUMN_WR_RX_CODE_ERROR,
       COLUMN_MS_EV_RATE,COLUMN_MS_DATA_RATE,
       COLUMN_DEV_SER, COLUMN_DEV_ID,
       COLUMN_UPTIME, COLUMN_LM_RESET,
       COLUMN_NUMBER};

const int REMOTE_CONTROL_PORT = 0; // 33392 adc64-system
const int WHOLE_TIMEOUT_MS = 15000;

const QMap<QString, quint64 MStreamStat::*> MS_WARN_MAP =
{
    {"fragDroped", &MStreamStat::fragDroped},
    {"fragMissed", &MStreamStat::fragMissed},
    {"missedEvents", &MStreamStat::missedEvents},
    {"missedEventsRanges", &MStreamStat::missedEventsRanges}
};
}

Q_DECLARE_METATYPE(Qt::GlobalColor)
Q_DECLARE_METATYPE(QtMsgType)

MainWindow::MainWindow(StartupOptions &startup_options, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    config(startup_options),
    remoteControl(new RemoteControlServer(this, REMOTE_CONTROL_PORT)),
    pnpServer(new PNPServer(this)),
    discoverDialog(new DiscoverDialog(this)),
    setupKey(1),
    statFlushTimer(new QTimer(this)),
    warnFlushTimer(new QTimer(this)),
    deviceSettingsDialog(new DeviceSettingsDialog(this)),
    evNumChecker(new EvNumChecker(this)),
    debug_mode(startup_options.debug_mode)
{
    thread()->setObjectName("GUI");
    qRegisterMetaType<MStreamStat>();
    qRegisterMetaType<QtMsgType>();
    qRegisterMetaType<Qt::GlobalColor>();
    qRegisterMetaType<AdcStatus>();
    qRegisterMetaType<DeviceDescription>();
    qRegisterMetaType<DeviceIndex>();
    qRegisterMetaType<DevFwRevMap>();
    qRegisterMetaType<DominoSettings>();
    qRegisterMetaType<RcRunId>();
    qsrand(QDateTime::currentDateTime().toTime_t());

    ui->setupUi(this);
    config.read_config();

    configuration_manager = new ConfigurationManager(this, startup_options.program_type, config.default_root);
    connect(configuration_manager, &ConfigurationManager::switch_to_config,
            this, &MainWindow::switch_to);

    objects_to_block.append(ui->actionAllow_Remote_Control);
    objects_to_block.append(ui->actionEnable_write);
    objects_to_block.append(ui->actionStop_run_on_TrigOnXOff);
    objects_to_block.append(ui->actionShow_details);

    connect(ui->actionQuit, &QAction::triggered,
            qApp, &QCoreApplication::quit);
    connect(evNumChecker, &EvNumChecker::completedEvNumCheck,
            remoteControl, &RemoteControlServer::evNumCheckResponse);
    connect(evNumChecker, &EvNumChecker::checkFail,
            this, &MainWindow::sendRunError);

    connect(remoteControl, &RemoteControlServer::startRequest,
            this, &MainWindow::onRemoteStartRequest);
    connect(remoteControl, &RemoteControlServer::stopRequest,
            this, &MainWindow::onRemoteStopRequest);
    connect(remoteControl, &RemoteControlServer::evNumCheckRequest,
            evNumChecker, &EvNumChecker::onRemoteEvNumCheckRequest);
    connect(this, &MainWindow::remoteStartCompleted,
            remoteControl, &RemoteControlServer::completedStart);
    connect(this, &MainWindow::remoteStopCompleted,
            remoteControl, &RemoteControlServer::completedStop);
    connect(this, &MainWindow::sendRemoteState,
            remoteControl, &RemoteControlServer::sendState);
    connect(remoteControl, &RemoteControlServer::connectionStatusUpdated,
            this, &MainWindow::remoteStatusUpdated);
    connect(this, &MainWindow::deleteAll,
            remoteControl, &RemoteControlServer::deleteLater);
    connect(this, &MainWindow::runError,
            remoteControl, &RemoteControlServer::onRunError);
    connect(this, &MainWindow::runWarning,
            remoteControl, &RemoteControlServer::onRunWarning);
    connect(this, &MainWindow::allowRemoteControl,
            remoteControl, &RemoteControlServer::startListen);
    connect(this, &MainWindow::config_transfer_to_rc,
            remoteControl, &RemoteControlServer::config_transfer);
    connect(this, &MainWindow::programDescriptionUpdated,
            remoteControl, &RemoteControlServer::changeProgramDescription);

    new ThreadController(remoteControl, this);

    QSet<quint16> devices;
    devices.insert(DEVICE_ID_ADC64);
    devices.insert(DEVICE_ID_ADC64S2);
    devices.insert(DEVICE_ID_ADC64VE);
    devices.insert(DEVICE_ID_ADC64VE_XGE);
    devices.insert(DEVICE_ID_ADC64ECAL);
    devices.insert(DEVICE_ID_ADC64WR);
    devices.insert(DEVICE_ID_ADC8BEDRS);
    devices.insert(DEVICE_ID_ADC8E);
    devices.insert(DEVICE_ID_EVADC);
    devices.insert(DEVICE_ID_TQDC16VS_E);
    devices.insert(DEVICE_ID_ADCM163);
    devices.insert(DEVICE_ID_ADC8E);

    discoverDialog->setDeviceIdFilter(devices);
    connect(discoverDialog, &DiscoverDialog::deviceDiscovered,
            this, &MainWindow::updateDeviceDiscoverInfo);

    new ThreadController(pnpServer, this);

    connect(this, &MainWindow::programDescriptionUpdated,
            pnpServer, &PNPServer::updateProgramDescription);
    connect(this, &MainWindow::closeProgram,
            pnpServer, &PNPServer::closeProgram);
    connect(this, &MainWindow::deleteAll,
            pnpServer, &PNPServer::deleteLater);

    connect(statFlushTimer, &QTimer::timeout,
            this, &MainWindow::flushMStreamStats);
    statFlushTimer->start(200);
    connect(warnFlushTimer, &QTimer::timeout,
            this, &MainWindow::flushWarnings);
    warnFlushTimer->setInterval(1000);
    warnFlushTimer->setSingleShot(true);

    QTimer::singleShot(0, this, &MainWindow::init);
}

MainWindow::~MainWindow()
{
    emit deleteAll();
    QElapsedTimer timer;
    timer.start();

    for(QThread *thread : adcThreads){
        if(!threadQuiting(thread, WHOLE_TIMEOUT_MS, &timer)){
            qDebug()<<"adc thread failed to quit quietly";
        }
    }

    for(QThread *thread : dumpersThread){
        if(!threadQuiting(thread, WHOLE_TIMEOUT_MS, &timer)){
            qDebug()<<"dumpers thread to quit quietly";
        }
    }
    delete discoverDialog;
}

void MainWindow::init()
{
    deviceSettingsDialog->setMStreamWrite(config.storage_path);
    deviceSettingsDialog->setMtuSize(config.mtu_size);
    deviceSettingsDialog->setStopConditions(config.event_number_limit);

    QString title = QString("%1    |    Program index: %2    |     Configuration name: %3")
        .arg(config.program_type).arg(config.program_index).arg(config.configuration_name);
    setWindowTitle(title);
    setTableLabels();

    QSettings settings;
    settings.beginGroup("MainWindow");
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("windowState").toByteArray());
    settings.endGroup();

    block_gui_signals(true);
    ui->table->setSortingEnabled(true);

    ui->actionWR_DAQ->setVisible(debug_mode);
    ui->actionAllow_Remote_Control->setChecked(config.allow_remote_control);
    emit allowRemoteControl(config.allow_remote_control);
    ui->actionEnable_write->setChecked(config.write_enable);
    ui->actionShow_details->setChecked(config.show_details);
    if(config.show_details) {
        ui->table->showColumn(COLUMN_UPTIME);
        ui->table->showColumn(COLUMN_LM_RESET);
    } else{
        ui->table->hideColumn(COLUMN_UPTIME);
        ui->table->hideColumn(COLUMN_LM_RESET);
    }
    ui->actionStop_run_on_TrigOnXOff->setChecked(config.trig_on_xoff_stop);
    block_gui_signals(false);

    updateProgramDescription();
    synch_result = config.synchronize_with_adc64();
    addDevices();
    resetAll(false);
}

void MainWindow::switch_to(QString program_index, QString configuration_name)
{
    configuration_manager->hide();
    config.program_index = program_index;
    config.configuration_name = configuration_name;

    ui->table->setRowCount(0);

    int tab_count = ui->tabWidget->count();
    for(int i=tab_count; i>0; --i){
        if(ui->tabWidget->tabText(i)!= "Main Log"){
            ui->tabWidget->removeTab(i);
        }
    }
    removeAllDevices();
    config.read_config();
    synch_result = config.synchronize_with_adc64();

    QTimer::singleShot(0, this, &MainWindow::init);
    warnFlushTimer->setSingleShot(true);
}

void MainWindow::on_actionConfiguration_manager_triggered()
{
    configuration_manager->show();
    configuration_manager->reset(config.program_index, config.configuration_name);
}

void MainWindow::on_actionSynchronize_configs_with_adc64_triggered()
{
    synch_result = config.synchronize_with_adc64();
}

void MainWindow::on_actionAbout_triggered()
{
    AboutBox::about();
}

void MainWindow::addLog(QtMsgType msgType, const QString &log, const QString &threadName)
{
    QTextCharFormat tf = ui->plainTextEdit->currentCharFormat();
    Qt::GlobalColor color = Qt::black;
    switch (msgType) {
    case QtFatalMsg:;
        break;
    case QtCriticalMsg:
        color = Qt::magenta;
        break;
    case QtWarningMsg:
        color = Qt::red;
        break;
    case QtInfoMsg:;
        color = Qt::blue;
        break;
    case QtDebugMsg:;
        break;
    }
    tf.setForeground(QBrush(color));
    ui->plainTextEdit->setCurrentCharFormat(tf);
    QString str = log;
    if(!threadName.isEmpty())
        str.prepend(QString("[thread:%1] ").arg(threadName));
    str.prepend(QString("[%1] ").arg(QTime::currentTime().toString()));
    ui->plainTextEdit->appendPlainText(str);
}


void MainWindow::on_actionWR_DAQ_triggered()
{
    WrDaqDialog dialog(&config, this);
    if(dialog.exec() != QDialog::Accepted)
        return;
}

void MainWindow::on_actionFindDevices_triggered()
{
    QVector<DeviceDescription> list;
    for(DominoSettings s : config.device_configs.values()) {
        DeviceDescription dd(s.devIndex);
        dd.enabled = s.enabled;
        list.push_back(dd);
    }
    discoverDialog->setSelected(list);
    if(discoverDialog->exec() != QDialog::Accepted)
        return;
    removeAllDevices();
    QVector<DeviceDescription> devices = discoverDialog->getSelectedDevices();
    for(DeviceDescription dd : devices) {
        qDebug()<<QString("Selected device: serial='%1'' f/w='%2' host='%3'")
                  .arg(dd.getSerialIdStr())
                  .arg(dd.getFirmWareStr())
                  .arg(QHostAddress(dd.ip_addr).toString());
    }
    config.device_configs.clear();
    for (DeviceDescription dd : devices) {
        DominoSettings &ds = config.device_configs[dd.getIndex()];
        ds.devIndex = dd.getIndex();
    }
    synch_result = config.synchronize_with_adc64();
    for (DeviceDescription dd : devices) {
        config.device_configs[dd.getIndex()].enabled = dd.enabled;
    }

    addDevices();
    resetAll();
    config.write_config();
    updateProgramDescription();
    updateRemoteStatus();
}

void MainWindow::on_actionClear_statistics_triggered()
{
    emit clearStatistics(DeviceIndex()); // to All
}

void MainWindow::on_actionAllow_Remote_Control_toggled(bool checked)
{
    config.allow_remote_control = checked;
    config.write_config();
    emit allowRemoteControl(config.allow_remote_control);
    updateProgramDescription();
}

void MainWindow::on_actionEnable_write_toggled(bool enabled)
{
    if(enabled){
        QString dir = QFileDialog::getExistingDirectory(this, tr("Storage Directory"),
                                                     config.storage_path,
                                                     QFileDialog::ShowDirsOnly
                                                     | QFileDialog::DontResolveSymlinks);
        if(dir.isEmpty()){
            ui->actionEnable_write->setChecked(false);
            return;
        }
        config.storage_path = dir;
        deviceSettingsDialog->setMStreamWrite(config.storage_path);
        qInfo() << QString("Changed storage dir to:").append(config.storage_path);
    }
    config.write_enable = enabled;
    qInfo() << QString("MStream write %1.").arg(config.write_enable? "enabled" : "disabled");
    config.write_config();
    updateProgramDescription();
}

void MainWindow::on_actionShow_details_triggered(bool show)
{
    config.show_details = show;
    config.write_config();
    if(show) {
        ui->table->showColumn(COLUMN_UPTIME);
        ui->table->showColumn(COLUMN_LM_RESET);
    } else{
        ui->table->hideColumn(COLUMN_UPTIME);
        ui->table->hideColumn(COLUMN_LM_RESET);
    }
}

void MainWindow::on_actionSet_stop_conditions_triggered()
{
    StopConditionsDialog dialog(&config, this);
    if(dialog.exec() == QDialog::Accepted){
        deviceSettingsDialog->setStopConditions(config.event_number_limit);
    }
}

void MainWindow::on_actionStop_run_on_TrigOnXOff_toggled(bool trig_on_xoff_stop)
{
    config.trig_on_xoff_stop = trig_on_xoff_stop;
    config.write_config();
}

void MainWindow::on_actionShow_device_settings_triggered()
{
    deviceSettingsDialog->show();
}

void MainWindow::on_actionSet_MTU_size_triggered()
{
    bool ok;
    int val = QInputDialog::getInt(this, "Set MTU size...", "MTU size:", config.mtu_size, 1500, 8000, 100, &ok);
    if(ok) {
        val &= ~3;
        if(config.mtu_size != val) {
            qInfo() << QString("MTU size changed from %1 to %2")
                    .arg(config.mtu_size).arg(val);
            config.mtu_size = static_cast<quint16>(val);
            deviceSettingsDialog->setMtuSize(config.mtu_size);
            resetAll(false);
            config.write_config();
        }
    }
}

void MainWindow::on_radioButtonStart_toggled(bool checked)
{
    DevFwRevMap adcFwRev;
    for(DeviceIndex &i : lastAdcStatus.keys()) {
        const AdcStatus &status = lastAdcStatus[i];
        adcFwRev[i] = status.fw_ver;
    }
    bool ok = true;
    if(!checked) {
        ok = phasingStop();
        if(!ok)
            qWarning() << "phasingStop failed";
    }
    setRunState(checked);
    if(!resetAll(true, false, checked)){
        qWarning("resetAll on on_radioButtonStart_toggled");
    }
    config.write_config();
}

void MainWindow::onRemoteStartRequest(const RcRunId &runId, quint64 start_event_number)
{
    ui->plainTextEdit->appendPlainText("");
    qInfo() << QString("Got remote request to start Run %1")
            .arg(QString(runId));
    Q_UNUSED(start_event_number)
    bool success=true;
    if(config.runId.runIndex == runId.runIndex &&
            config.runId.runNumber > runId.runNumber) {
        qWarning() << QString("Previous runNumber(%1) is greater that remote control runNumber(%2)")
                      .arg(config.runId.runNumber)
                      .arg(runId.runNumber);
    }

    if(config.run) {
        setRunState(false);
        success &= resetAll(false, false, true);
    }

    if(success) {
        config.runId.set(runId);
        if(!config.run) {
            setRunState(true);
            success &= resetAll(true, false, true);
            emit clearStatistics(DeviceIndex());
        }
    }

    if(success) {
        DevFwRevMap adcFwRev;
        for(DeviceIndex &i : lastAdcStatus.keys()) {
            const AdcStatus &status = lastAdcStatus[i];
            adcFwRev[i] = status.fw_ver;
        }
//      success &= dbOk;
    }

    if(!success) {
        qWarning() << "Something was wrong while executing remote start";
    } else {
        qInfo() << QString("Remote start completed successfully for run %1")
                   .arg(QString(runId));
    }

    emit remoteStartCompleted(success, runId);
    if(success){
        emit config_transfer_to_rc(config.update_root().get_json_data());
    }
    updateProgramDescription();
    config.write_config();
}

void MainWindow::onRemoteStopRequest(const RcRunId &runId, quint64 start_event_number)
{
    Q_UNUSED(start_event_number)
    bool success=true;
    if(config.runId.runIndex != runId.runIndex) {
        qWarning()<< QString("Got stop request with wrong run index: runIndex (%1); remote control runIndex (%2)")
                     .arg(config.runId.runIndex)
                     .arg(runId.runIndex);
        success = false;
    }
    if(success && config.runId.runNumber != runId.runNumber){
        qWarning()<< QString("Got stop of unknown run: runNumber (%1); remote control runNumber (%2)")
                     .arg(config.runId.runNumber)
                     .arg(runId.runNumber);
        success = false;
    }
    if(success) {
        if(config.run) {
            success &= phasingStop();
            if(success)
                qWarning() << "phasingStop failed";
            setRunState(false);
            success &= resetAll(true, false, false);
        } else {
            qWarning() << "Can't execute remote stop (system is already stopped).";
            success = false;
        }
    }

    if(!success) {
        qWarning("Something was wrong while executing remote stop");
    } else {
        qInfo("Remote stop completed successfully");
    }

    emit remoteStopCompleted(success, runId);
    updateProgramDescription();
    config.write_config();
}

void MainWindow::adcConnected()
{
    const DeviceIndex &index = devices.key(reinterpret_cast<DominoDevice*>(sender()));

    if(index.isNull())
        return;
    adcStateChanged(index, true);
}

void MainWindow::adcDisconnected()
{
    const DeviceIndex &index = devices.key(reinterpret_cast<DominoDevice*>(sender()));
    if(index.isNull())
        return;
    adcStateChanged(index, false);
}

void MainWindow::adcStateChanged(const DeviceIndex &index, bool online)
{
    if(online) {
        const QString serialStr = index.getSerialStr(); // QString("0x%1").arg(index.getSerial(), 8, 16, QChar('0'));
        qInfo() << QString("[%1] Device online").arg(serialStr);

        sendAdcSetup(index);
        emit mstream_send_connect(index);
    } else {
        deviceSetupKeys.remove(index);
        updateRemoteStatus();
        auto mess = QString("[%1] Device offline").arg(index.getSerialStr());
        sendRunError(mess);
    }

    const int row = getRowByIndex(index);
    Q_ASSERT(row!=-1);
    const QColor bgColorDefault = ui->table->style()->standardPalette().light().color();
    for(int col=0;col<COLUMN_NUMBER;++col){
        ui->table->item(row, col)->setBackgroundColor(online ? bgColorDefault : Qt::lightGray);
    }
}


void MainWindow::updateAdcStatus(const DeviceIndex &index, const AdcStatus &status)
{
    const int row = getRowByIndex(index);
    if(row==-1)
        return;
    if (!devices.contains(index)) return;
    if(devices[index] != sender()) {
        // Got status from previous instance of hw
        // DominoDevice was recreated
        return;
    }

    const QColor bgColorDefault = ui->table->style()->standardPalette().light().color();
    const QColor bgColorWarning = Qt::yellow;
    const QColor bgColorError = Qt::red;

    QTableWidget * const table = ui->table;
    table->setUpdatesEnabled(false);
    // read temperature sensors
    {
        QStringList sl;
        if (status.temp > -255)
            sl += QString("ADC:%1").arg((int)status.temp);
        QMap<int, QString> name;
        name[0] = "PS";
        name[1] = "AMP";
        for (int i=0; i<status.bmStatus.sensors.size(); i++) {
            double t = status.bmStatus.sensors[i].temp;
            if (t > -255)
                sl += QString("%1:%2").arg(name[i]).arg((int)t);
        }
        table->item(row, COLUMN_TEMP)->setText(sl.join(" "));
    }
    table->item(row, COLUMN_EVENT)->setText(QString::number(status.evNum));

    if(!status.liveMagicCorrect && config.run) {
        sendRunError("liveMagic incorrect");
    }
    if(!status.adcSelfTestOk && config.run) {
        sendRunError("AdcSelfTest failed");
    }
    if(config.trig_on_xoff_stop && status.trigOnXOff && config.run) {
        sendRunError("Trig on XOff detected");
    }

    if(synch_result.contains(getIndexByRow(row))){
       if(!synch_result[getIndexByRow(row)]){
           ui->table->item(row, COLUMN_SYNCH_STATUS)->setText("Error");
           ui->table->item(row, COLUMN_SYNCH_STATUS)->setBackgroundColor(QColor(255, 153, 153));
       }
       else{
           ui->table->item(row, COLUMN_SYNCH_STATUS)->setText("Ok");
           ui->table->item(row, COLUMN_SYNCH_STATUS)->setBackgroundColor(QColor(102, 204, 102));
       }
    }

    table->item(row, COLUMN_ADC_STATE_OK)->setText(status.adcSelfTestOk?"Ok":QString("Error: %1").arg(status.adcDesStatus,8,2,QChar('0')));
    table->item(row, COLUMN_ADC_STATE_OK)->setBackgroundColor(status.adcSelfTestOk ? bgColorDefault : bgColorError);

    table->item(row, COLUMN_WR_LINK_ERROR)->setText(QString::number(status.wrEpStatus.wrLinkError));
    table->item(row, COLUMN_WR_SYNC_LOST)->setText(QString::number(status.wrEpStatus.wrSyncFail));

    bool wrTimeValid = false;
    const WrEpStatus &wrEpStatus = status.wrEpStatus;
    if (!wrEpStatus.hasWrEpStatus) {
        wrTimeValid = status.adcStatus & 0x2;
        table->item(row, COLUMN_WR_TIME)->setText(wrTimeValid?"Ok":"Invalid");
        table->item(row, COLUMN_WR_TIME)->setBackgroundColor(wrTimeValid ? bgColorDefault : bgColorError);
        table->item(row, COLUMN_WR_RX_CODE_ERROR)->setText("N/A");
        table->item(row, COLUMN_WR_TIME_OFFSET)->setText("N/A");
        table->item(row, COLUMN_WR_TIME_OFFSET)->setBackgroundColor(bgColorDefault);
    } else {
        wrTimeValid = status.wrEpStatus.timeValid();
        const bool wrTimeNull = status.wrEpStatus.timeNull();
        if (wrTimeNull) {
            table->item(row, COLUMN_WR_TIME)->setText(QString());
            table->item(row, COLUMN_WR_TIME)->setBackgroundColor(bgColorWarning);
            table->item(row, COLUMN_WR_TIME_OFFSET)->setText(QString());
            table->item(row, COLUMN_WR_TIME_OFFSET)->setBackgroundColor(bgColorWarning);
        } else {
            QDateTime wrTime = QDateTime::fromMSecsSinceEpoch(status.wrEpStatus.mSecsSinceEpoch());

            int wrTimeCheck = status.wrEpStatus.wrTimeCheck();
            double wrTimeOffsetMs = status.wrEpStatus.mSecsSinceEpoch() - status.wrEpStatus.acquiredAtMs;
            QColor offsetBgColor = bgColorDefault;
            if (wrTimeCheck == 1)
                offsetBgColor = bgColorWarning;
            if (wrTimeCheck > 1)
                offsetBgColor = bgColorError;

            table->item(row, COLUMN_WR_TIME)->setText(wrTime.toUTC().toString("yyyy-MM-dd hh:mm:ss")); // add ".zzz" for ms
            table->item(row, COLUMN_WR_TIME)->setBackgroundColor(wrTimeValid ? bgColorDefault : bgColorError);
            QString strOffset;
            if (wrTimeOffsetMs/1000. > -60 && wrTimeOffsetMs/1000. < 60)
                strOffset = QString(" %1").arg(wrTimeOffsetMs * 1e-3, 6, 'f', 3);
            table->item(row, COLUMN_WR_TIME_OFFSET)->setText(strOffset);
            table->item(row, COLUMN_WR_TIME_OFFSET)->setBackgroundColor(offsetBgColor);
        }
        table->item(row, COLUMN_WR_RX_CODE_ERROR)->setText(QString::number(status.wrEpStatus.wrCodeError));
        table->item(row, COLUMN_UPTIME)->setText(QString::number(status.wrEpStatus.wrUptime));
    }
    table->item(row, COLUMN_LM_RESET)->setText(QString::number(status.liveMagicResetCount));

    table->item(row, COLUMN_TRIG_ON_XOFF)->setText(QString::number(status.trigOnXOff));

    //    if(hw->isConnected() && !status.liveMagicCorrect){
    //        AdcSetup &setup = systemSetup.adc[cfgIndex];
    //        qWarning() << QString("[0x%2] Warning: Device Adc-%1 failed magic word check. Resetting latest configuration. (%3)")
    //                      .arg(cfgIndex).arg(setup.serialId, 8, 16, QChar('0'))
    //                      .arg(setup.host.toString());
    //        addProcessLog(row, "==== RESET ====");
    //        dumpers[row]->restart();
    //    }
    if(config.run && config.event_number_limit){
        bool stop = true;
        for(int row=0; row < table->rowCount(); ++row){
            bool ok;
            quint64 evCnt = table->item(row, COLUMN_EVENT)->text().toULongLong(&ok);
            stop &= evCnt > config.event_number_limit;
            stop &= ok;
        }
        if(stop){
            qInfo() << QString("Stopping by stop condition: evNumLimit=%1").arg(config.event_number_limit);
            ui->radioButtonStop->setChecked(true);
        }
    }
    table->setUpdatesEnabled(true);

    const AdcStatus prevStatus = lastAdcStatus.value(index);
    const bool dumperRestart =
            prevStatus.mstreamVer2 !=status.mstreamVer2 ||
            prevStatus.msMultiAck !=status.msMultiAck ||
            prevStatus.hwBufSize !=status.hwBufSize;
    lastAdcStatus[index] = status;
    if(dumpers.contains(index)){
        if(dumperRestart) {
            resetMStream(index);
        }
    } else {
        addMStreamDump(index);
    }
}

void MainWindow::updateDeviceDiscoverInfo(const DeviceDescription &dd)
{
    int row = getRowByIndex(dd.getIndex());
    if(row == -1){
        return;
    }

    Q_ASSERT(config.device_configs.contains(dd.getIndex()));
    QHostAddress newHost(dd.ip_addr);
    QString prevHostStr = ui->table->item(row, COLUMN_IP)->text();
    QString newHostStr = newHost.toString();

    if(prevHostStr != newHostStr) {
        if(prevHostStr.isEmpty())
            qDebug() << QString("[%1] host address set %2")
                        .arg(dd.getIdent()).arg(newHostStr);
        else
            qInfo() << QString("[%1] host changed from %2 to %3")
                       .arg(dd.getIdent()).arg(prevHostStr).arg(newHostStr);

        ui->table->item(row, COLUMN_IP)->setText(newHostStr);
    }
    ui->table->item(row, COLUMN_SLOT)->setText(QString::number(dd.chassis_slot));
}

void MainWindow::adcSetupWritten(const DominoSettings &s)
{
    const DeviceIndex &index = devices.key(reinterpret_cast<DominoDevice*>(sender()));
    if(index.isNull())
        return;

    const bool send = (!deviceSetupKeys.contains(index) && s.key==setupKey) ||
            ((deviceSetupKeys[index]!=setupKey) == ((s.key!=setupKey)));
    deviceSetupKeys[index] = s.key;
    if(send)
        updateRemoteStatus();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings;
    settings.beginGroup("MainWindow");
    settings.setValue("geometry", saveGeometry());
    settings.setValue("windowState", saveState());
    settings.endGroup();
    QMainWindow::closeEvent(event);
}

void MainWindow::addDevices()
{
    ui->actionWR_DAQ->setEnabled(!config.device_configs.isEmpty());

    ui->table->blockSignals(true);
    int row=ui->table->rowCount();
    Q_ASSERT(row==0);

    for (DeviceIndex index : config.device_configs.keys()) {
        DominoSettings& ds = config.device_configs[index];
        if (!ds.enabled) continue;

        // Insert a row to table
        ui->table->setRowCount(row+1);
        for(int col=0; col<COLUMN_NUMBER; ++col) {
            ui->table->setItem(row, col, new QTableWidgetItem());
            Qt::ItemFlags eFlags = ui->table->item(row, col)->flags();
            eFlags &= ~Qt::ItemIsEditable;
            ui->table->item(row, col)->setFlags(eFlags);
        }

        ui->statistic_widget->add_device(index);

        // Add device
        QThread *separateThread = new QThread();
        adcThreads[ds.devIndex] = separateThread;
        deviceSetupKeys.remove(ds.devIndex);
        separateThread->setObjectName(ds.devIndex.getIdent());
        separateThread->start(QThread::LowPriority);

        DominoDevice *hw = DominoDeviceFactory::create(ds.devIndex, ds.hwStr);
        connect(discoverDialog, &DiscoverDialog::deviceDiscovered,
                hw, &DominoDevice::deviceUpdated);
        connect(hw, &DominoDevice::deviceConnected,
                this, &MainWindow::adcConnected);
        connect(hw, &DominoDevice::deviceDisconnected,
                this, &MainWindow::adcDisconnected);
        connect(hw, &DominoDevice::statusUpdated,
                this, &MainWindow::updateAdcStatus);
        connect(this, &MainWindow::deleteHardware,
                hw, &DominoDevice::deleteHardware);
        connect(this, &MainWindow::deleteAll,
                hw, &DominoDevice::deleteLater);
        connect(this, &MainWindow::clearStatistics,
                hw, &DominoDevice::clearStatistics);
        connect(this, &MainWindow::writeAdcSetup,
                hw, &DominoDevice::on_setupChanged);
        connect(hw, &DominoDevice::setupWritten,
                this, &MainWindow::adcSetupWritten);
        connect(hw, &DominoDevice::destroyed,
                separateThread, &QThread::quit);
        connect(evNumChecker, &EvNumChecker::evNumRequest,
                hw, &DominoDevice::evNumRequest);
        connect(hw, &DominoDevice::evNumResponse,
                evNumChecker, &EvNumChecker::gotDevEvNum);
        ds.setNCh(hw->getNumberOfChannels());
        hw->moveToThread(separateThread);
        devices[ds.devIndex] = hw;
        qDebug() << QString("[%1] started device thread")
                    .arg(ds.devIndex.getIdent());

        // Pre-connection setup
        ui->table->item(row, COLUMN_DEV_ID_STR)->setText(getDeviceTypeName(ds.devIndex.getDevId()));
        ui->table->item(row, COLUMN_SERIAL_STR)->setText(ds.devIndex.getSerialStr());
        ui->table->item(row, COLUMN_DEV_SER)->setText(QString::number(ds.devIndex.getSerial()));
        ui->table->item(row, COLUMN_DEV_ID)->setText(QString::number(ds.devIndex.getDevId()));

        adjustTableHeight(ui->table);
        // print what will be default setup for adc64
        qDebug() << QString("[%1] ==== Default configuration on start-up ====\r%2")
                    .arg(ds.devIndex.getIdent())
                    .arg(ds.toString());
        emit writeAdcSetup(ds);
        ++row;
    }
    deviceSettingsDialog->deviceListUpdated(config.device_configs);

    ui->table->blockSignals(false);

    updateRemoteStatus();
//    evNumChecker->setupUpdated(&config);
    QVector<quint64> serials;
    for(auto ds : config.device_configs){
        if(ds.enabled)
            serials.append(ds.devIndex.getSerial());
    }
    evNumChecker->set_dev_serials(serials);
}

void MainWindow::removeAllDevices()
{
    qDebug("Removing all devices");
    //Init stopping all

    for(DominoSettings ds : config.device_configs.values()) {
        emit deleteHardware(ds.devIndex);
    }
    QElapsedTimer timer;
    timer.start();

    for(QThread *thread : adcThreads){
        if(!threadQuiting(thread, WHOLE_TIMEOUT_MS, &timer)){
            qWarning() << QString("Thread %1 failed to quit").arg(thread->objectName());
        }
        delete thread;
    }

    for(QThread *thread : dumpersThread){
        if(!threadQuiting(thread, WHOLE_TIMEOUT_MS, &timer)){
            qWarning() << QString("Thread %1 failed to quit").arg(thread->objectName());
        }
        delete thread;
    }

    adcThreads.clear();
    devices.clear();
    deviceSetupKeys.clear();

    dumpers.clear();
    dumpersThread.clear();
    lastAdcStatus.clear();

    ui->table->setRowCount(0);

    ui->statistic_widget->remove_all_devices();
}

void MainWindow::setRunState(bool run)
{
    config.run = run;
    emit set_run_state(run);
    ui->radioButtonStart->blockSignals(true);
    if(run)
        ui->radioButtonStart->setChecked(true);
    else
        ui->radioButtonStop->setChecked(true);
    ui->radioButtonStart->blockSignals(false);

    ui->actionWR_DAQ->setEnabled(!run);
    ui->actionFindDevices->setEnabled(!run);
    ui->actionAllow_Remote_Control->setEnabled(!run);
    ui->actionEnable_write->setEnabled(!run);
    ui->actionSet_stop_conditions->setEnabled(!run);
    ui->actionSet_MTU_size->setEnabled(!run);
    updateProgramDescription();
}

void MainWindow::updateRemoteStatus()
{
    QStringList readyStr, notReadyStr;
    bool ready4Run = true;
    for(DominoSettings &ds : config.device_configs.values()) {
        if(!ds.enabled)
            continue;
        const bool devOk = deviceSetupKeys.value(ds.devIndex)==setupKey
                && lastAdcStatus.contains(ds.devIndex)
                && lastAdcStatus[ds.devIndex].adcSelfTestOk;
        if(devOk) {
            readyStr << QString("0x%1 ").arg(ds.devIndex.getSerial(), 8, 16, QChar('0'));
        } else {
            notReadyStr << QString("0x%1 ").arg(ds.devIndex.getSerial(), 8, 16, QChar('0'));
            ready4Run = false;
        }
    }

    QString stateStr;
    if(!notReadyStr.isEmpty()) {
        auto str = notReadyStr.join(" ");
        if(str.size() > 40) {
            stateStr.append("Not ready:").append(QString("%1 device(s)").arg(notReadyStr.count()));
        } else {
            stateStr.append("Not ready:").append(str);
        }
    }
    if(!readyStr.isEmpty()) {
        if(!stateStr.isEmpty())
            stateStr.append("| ");
        auto str = readyStr.join(" ");
        if(str.size() > 40) {
            stateStr.append("Ready:").append(QString("%1 device(s)").arg(readyStr.count()));
        } else {
            stateStr.append("Ready:").append(str);
        }
    }
    if(stateStr.isEmpty())
        stateStr = "No devices";
    emit sendRemoteState(stateStr, ready4Run);
}

void MainWindow::remoteStatusUpdated(const RemoteControlServerStatus &status)
{
    if(!program_description.containsProgramInterface(ProgramInterfaceRemoteControl, 0)) {
        ProgramInterface interface;
        interface.setType(ProgramInterfaceRemoteControl);
        interface.port = 0;
        interface.enabled = config.allow_remote_control;
        interface.isFree = false;
        program_description.interfaces.push_back(interface);
    }

    ProgramInterface &i = program_description.getProgramInterface(ProgramInterfaceRemoteControl);
    ProgramInterface prevIt = i;
    i.port = status.tcpPort;
    i.enabled = status.listening;
    i.isFree = !status.portInUse;

    if(status.portInUse) {
        i.setPeer(status.peerHost, status.peerPort);
    } else {
        i.peers.clear();
    }

    if(prevIt != i)
        updateRemoteStatus();
    ui->groupBoxRunControl->setEnabled(!status.portInUse);
    updateProgramDescription();
}

void MainWindow::sendRunError(QString mess)
{
    qWarning() << mess;
    if(config.run && config.allow_remote_control)
        emit runError(config.runId, mess);
}

void MainWindow::sendAdcSetup(const DeviceIndex &index)
{
    if(!config.device_configs.contains(index))
        return;
    DominoSettings &ds = config.device_configs[index];
    ds.mtuSize = config.mtu_size;
    ds.run = config.run;
    ds.key = setupKey;

    emit writeAdcSetup(ds);
}

bool MainWindow::resetAll(bool doResetMStream, bool doEvNumCheck, bool doStatClear)
{
    deviceSetupKeys.clear();
    if(++setupKey==0) ++setupKey;
    config.run_time_str = QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss");

    bool waitFlag = false;
    for(DeviceIndex index : devices.keys()) {
        if(config.run && doResetMStream) {
            // TODO: do we should reseat MStream?
            resetMStream(index, doStatClear);
            waitFlag = true;
        }
    }
    if(waitFlag)
        QThread::currentThread()->msleep(500);
    for(DeviceIndex index : devices.keys()) {
        sendAdcSetup(index);
    }

    QElapsedTimer timer;
    timer.start();
    bool ok=waitDevSetupKey(timer, 1000);
    if(!ok) {
        qWarning() << "deviceSetupKeys doesn't fit to setupKey";
    }
    if(ok && doEvNumCheck) {
        ok = waitDevEvNum(timer, 1000);
        if(!ok)
            qWarning() << "check for lastEvNum failed";
    }

    if(!config.run && doResetMStream) {
        for(DeviceIndex index : devices.keys()) {
            resetMStream(index, doStatClear);
        }
    }

    updateRemoteStatus();
    if(!ok) {
        readyForRun(true);
    } else {
        qDebug("ResetAll finished successfully");
    }
    return ok;
}

DeviceIndex MainWindow::getIndexByRow(int row) const
{
    return DeviceIndex(ui->table->item(row, COLUMN_DEV_ID)->text().toUInt(),
                       ui->table->item(row, COLUMN_DEV_SER)->text().toUInt());
}

int MainWindow::getRowByIndex(const DeviceIndex &index) const
{
    for(int row=0; row<ui->table->rowCount(); ++row){
        if(getIndexByRow(row)==index)
            return row;
    }
    return -1;
}

void MainWindow::updateProgramDescription()
{
    program_description.type = config.program_type;
    program_description.index = config.program_index;
    program_description.name = program_description.getName();

#ifdef GIT_DESCR
    program_description.ver_hash = GIT_DESCR;
#endif
#ifdef GIT_LAST_COMMIT
    program_description.ver_date = GIT_LAST_COMMIT;
#endif

    program_description.options.clear();
    QString run_str = config.run ? "true" : "false";
    program_description.options["run"] = run_str;
    program_description.options["run_index"] = config.runId.runIndex;
    program_description.options["run_number"] = QString::number(config.runId.runNumber);
    QString write_enable_str = config.write_enable ? "true" : "false";
    program_description.options["write_enable"] = write_enable_str;

    int i=0;
    for(DeviceIndex &index : config.device_configs.keys()){
        if(!config.device_configs[index].enabled)
            continue;
        program_description.options[QString("adc_%1_id").arg(i)] = QString("0x%1")
                .arg(index.first, 0, 16);
        program_description.options[QString("adc_%1_serial").arg(i)] = QString("0x%1")
                .arg(index.second, 0, 16);
        ++i;
    }

    emit programDescriptionUpdated(program_description);
}

bool MainWindow::readyForRun(bool printErr) const
{
    bool ready = true;
    for(DeviceIndex &index : devices.keys()){
        if(deviceSetupKeys.value(index, 0) != setupKey){
            if(printErr)
                qWarning() << QString("[%1] at row %2 failed to write setup")
                              .arg(index.getIdent())
                              .arg(getRowByIndex(index));
            ready = false;
        }
    }
    return ready;
}

bool MainWindow::waitDevSetupKey(const QElapsedTimer &timer, int timeout)
{
    bool ok = false;
    while(!ok && !timer.hasExpired(timeout)) {
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 50);
        ok=true;
        for(DominoSettings &ds : config.device_configs.values()) {
            if(!ds.enabled) continue;
            if(deviceSetupKeys.value(ds.devIndex, 0) != setupKey) {
                ok=false;
                break;
            }
        }
    }
    return ok;
}

bool MainWindow::waitDevEvNum(const QElapsedTimer &timer, int timeout)
{
    const QString LAST_EV_NUM_FIELD = "lastEv";
    bool ok = false;
    while(!ok && !timer.hasExpired(timeout)) {
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 50);
        ok=true;
        for(DominoSettings &ds : config.device_configs.values()) {
            if(!ds.enabled) continue;

            const DeviceIndex &i = ds.devIndex;
            QString serial = i.getSerialStr();
            QMap<DeviceIndex, MStreamStat> stats = ui->statistic_widget->getStat();
            if(!lastAdcStatus.contains(i) ||
               !stats.contains(i) ||
               stats[i].lastEvNum != lastAdcStatus[i].evNum ||
               !ok) {
                ok=false;
                break;
            }
        }
    }
    return ok;
}

bool MainWindow::phasingStop()
{
    QMap<DeviceIndex, quint16> all_map;
    QMap<DeviceIndex, bool> skipRestartRun_map;

    for (DeviceIndex index : config.device_configs.keys()) {
        DominoSettings &ds = config.device_configs[index];
        if(!ds.enabled)
            continue;
        all_map[index] = ds.trigsetup.all;
        skipRestartRun_map[index] = ds.skipRestartRun;

        ds.trigsetup.all = 0;
        ds.skipRestartRun = true;
    }

    bool ok = false;
    ok = resetAll(false, true);

    for (DeviceIndex index : config.device_configs.keys()) {
        DominoSettings &ds = config.device_configs[index];
        if(!ds.enabled)
            continue;
        ds.trigsetup.all = all_map[index];
        ds.skipRestartRun = skipRestartRun_map[index];
    }
    return ok;
}

void MainWindow::adjustTableHeight(QTableWidget *table)
{
    int height=table->horizontalHeader()->height();
    for(int i=0;i<table->rowCount();++i){
        height += table->rowHeight(i);
    }
    height += 5;
    table->setMinimumHeight(height);
    QRect geo = table->geometry();
    geo.setHeight(height);
    table->setGeometry(geo);
}

void MainWindow::setTableLabels()
{
    ui->table->setColumnCount(COLUMN_NUMBER);
    ui->table->setHorizontalHeaderItem(COLUMN_DEV_ID_STR, new QTableWidgetItem("Type"));
    ui->table->setHorizontalHeaderItem(COLUMN_SERIAL_STR, new QTableWidgetItem("Serial"));
    ui->table->setHorizontalHeaderItem(COLUMN_SLOT, new QTableWidgetItem("Slot"));
    ui->table->setHorizontalHeaderItem(COLUMN_IP, new QTableWidgetItem("IP Address"));
    ui->table->setHorizontalHeaderItem(COLUMN_TEMP, new QTableWidgetItem(QString("t, ")+QChar(0x02da)+"C"));
    ui->table->setHorizontalHeaderItem(COLUMN_EVENT, new QTableWidgetItem("Event"));
    ui->table->setHorizontalHeaderItem(COLUMN_TRIG_ON_XOFF, new QTableWidgetItem("Trig on XOff"));
    ui->table->setHorizontalHeaderItem(COLUMN_SYNCH_STATUS, new QTableWidgetItem("Synch to ADC64"));
    ui->table->setHorizontalHeaderItem(COLUMN_ADC_STATE_OK, new QTableWidgetItem("ADC Status"));
    ui->table->setHorizontalHeaderItem(COLUMN_WR_TIME, new QTableWidgetItem("WR Time"));
    ui->table->setHorizontalHeaderItem(COLUMN_WR_TIME_OFFSET, new QTableWidgetItem("Offset"));
    ui->table->setHorizontalHeaderItem(COLUMN_WR_LINK_ERROR, new QTableWidgetItem("Link"));
    ui->table->setHorizontalHeaderItem(COLUMN_WR_SYNC_LOST, new QTableWidgetItem("Unlock"));
    ui->table->setHorizontalHeaderItem(COLUMN_WR_RX_CODE_ERROR, new QTableWidgetItem("RX Err"));
    ui->table->setHorizontalHeaderItem(COLUMN_MS_EV_RATE, new QTableWidgetItem("Event rate"));
    ui->table->setHorizontalHeaderItem(COLUMN_MS_DATA_RATE, new QTableWidgetItem("Data rate"));
    ui->table->setHorizontalHeaderItem(COLUMN_DEV_SER, new QTableWidgetItem("Serial Num"));
    ui->table->setHorizontalHeaderItem(COLUMN_DEV_ID, new QTableWidgetItem("type"));
    ui->table->setHorizontalHeaderItem(COLUMN_UPTIME, new QTableWidgetItem("up time"));
    ui->table->setHorizontalHeaderItem(COLUMN_LM_RESET, new QTableWidgetItem("LM resets"));
    ui->table->hideColumn(COLUMN_DEV_SER);
    ui->table->hideColumn(COLUMN_DEV_ID);
    if(!config.show_details){
        ui->table->hideColumn(COLUMN_UPTIME);
        ui->table->hideColumn(COLUMN_LM_RESET);
    }

//    ui->table->setHorizontalHeaderLabels(columnHeaders);
#if QT_VERSION >= 0x050000
    ui->table->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
#else
    ui->table->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
#endif
}

void MainWindow::addMStreamDump(const DeviceIndex &index)
{
    MStreamDumpParameters dump_parameters;
    auto adcStatus = lastAdcStatus.value(index);
    dump_parameters.deviceIndex = index;
    const auto dd = discoverDialog->getDescriptionByIndex(index);
    if(dd.getIndex() == index && dd.ip_addr){
        dump_parameters.deviceAddress = QHostAddress(dd.ip_addr);
    }
    Q_ASSERT(dd.ip_addr);
    dump_parameters.fileName = getMStreamFileName(index);
    dump_parameters.bigFragId = adcStatus.mstreamVer2;
    dump_parameters.hwBufSize = adcStatus.hwBufSize;
    dump_parameters.ackSizeLimit = adcStatus.msMultiAck ? adcStatus.hwBufSize : 1;
    dump_parameters.preRunInit();

    MStreamDump *dump = dumpers[index] = new MStreamDump(dump_parameters, false);
    connect(dump, &MStreamDump::closing,
            pnpServer, &PNPServer::closeProgram);
    connect(dump, &MStreamDump::updateProgramDescription,
            pnpServer, &PNPServer::updateProgramDescription);
    connect(dump, &MStreamDump::stat_updated,
            ui->statistic_widget, &StatisticOutput::stat_updated);
    connect(discoverDialog, &DiscoverDialog::deviceDiscovered,
            dump, &MStreamDump::receivedMldpDescr);
    connect(dump, &MStreamDump::stat_from_receiver_updated,
            ui->statistic_widget, &StatisticOutput::stat_from_receiver_updated);
    connect(this, &MainWindow::msdump_reset,
            dump, &MStreamDump::msdump_reset);
    connect(this, &MainWindow::mstream_send_connect,
            dump, &MStreamDump::send_connect_to_dev);
    connect(this, &MainWindow::set_run_state,
            ui->statistic_widget, &StatisticOutput::set_run_state);

    QThread *thread = dumpersThread[index] = new QThread(this);
    thread->setObjectName("MSD_"+index.getSerialStr());
    connect(dump, &MStreamDump::destroyed,
            thread, &QThread::quit);
    connect(this, &MainWindow::deleteHardware,
            dump, &MStreamDump::delete_dump);
    connect(this, &MainWindow::deleteAll,
            dump, &MStreamDump::deleteLater);
    dump->moveToThread(thread);
    thread->start();
}

void MainWindow::resetMStream(const DeviceIndex &index, bool doStatClear)
{
    if(!config.device_configs.contains(index))
        return;
    DominoSettings &ds = config.device_configs[index];
    if(!ds.enabled)
        return;

    auto adcStatus = lastAdcStatus.value(index);

    QString fn = getMStreamFileName(index);
    emit msdump_reset(index, adcStatus.mstreamVer2, adcStatus.hwBufSize,
                     adcStatus.msMultiAck ? adcStatus.hwBufSize : 1, fn, doStatClear);
}

void MainWindow::flushMStreamStats()
{
    QJsonDocument mainDoc;
    QFile mainFile(QString("%1%2%3_list.txt")
                   .arg(QDir::tempPath())
                   .arg(QDir::separator())
                   .arg(QApplication::applicationName()) );
    QJsonArray mainArray;
    for(DeviceIndex &index : mstreamStats.keys()) {
        mainArray.push_back(QJsonValue(index.getSerialStr()));

        QFile devFile(QString("%1%2%3_%4.txt")
                       .arg(QDir::tempPath())
                       .arg(QDir::separator())
                       .arg(QApplication::applicationName())
                      .arg(index.getSerialStr()) );
        if(!devFile.open(QIODevice::WriteOnly)){
            qWarning()<<"Failed to open temp file:"<<devFile.fileName();
            continue;
        }
        QJsonDocument devDocument;
        QJsonObject devContent;
        for(QString &key : mstreamStats[index].keys()) {
            devContent.insert(key, mstreamStats[index][key]);
        }
        devDocument.setObject(devContent);
        devFile.write(devDocument.toJson(QJsonDocument::Indented));
        devFile.close();
    }
    mainDoc.setArray(mainArray);
    if(!mainFile.open(QIODevice::WriteOnly)){
        qWarning()<<"Failed to open temp file:"<<mainFile.fileName();
        return;
    }
    mainFile.write(mainDoc.toJson(QJsonDocument::Indented));
    mainFile.close();
}

void MainWindow::flushWarnings()
{
    auto stats = ui->statistic_widget->getStat();
    QStringList warnStr;
    for(auto serial : stats.keys()) {
        auto stat = stats[serial];
        QString devWarnStr;
        for(auto key : MS_WARN_MAP.keys()) {
            quint64 val = stat.*MS_WARN_MAP[key];
            if(val==0)
                continue;
            if(devWarnStr.isEmpty()) devWarnStr = QString("[%1] ").arg(serial.getSerialStr());
            warnStr.append(QString("%1=%2 ").arg(key).arg(val));
        }
    }

    emit runWarning(config.runId, warnStr.join(' '));
    warnFlushTimer->start();
}

void MainWindow::block_gui_signals(bool block)
{
    for(int i=0; i<objects_to_block.size(); ++i){
        objects_to_block[i]->blockSignals(block);
    }
}

QString MainWindow::getMStreamFileName(DeviceIndex index) const
{
    QString fn;
    if(config.run && config.write_enable) {
        QString folderPrefix = config.storage_path;
        if(!folderPrefix.isEmpty())
            folderPrefix.append(QDir::separator());

        fn = folderPrefix.append(QString("%2_%3.data")
                                         .arg(index.getSerial(),8,16,QChar('0'))
                                         .arg(config.run_time_str));
    }
    return fn;
}
