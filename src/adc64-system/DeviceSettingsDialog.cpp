#include "DeviceSettingsDialog.h"
#include "ui_DeviceSettingsDialog.h"

#include "mregdev/device_id.h"

DeviceSettingsDialog::DeviceSettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DeviceSettingsDialog)
{
    ui->setupUi(this);
    setWindowTitle("Device settings");
}

DeviceSettingsDialog::~DeviceSettingsDialog()
{
    delete ui;
}

void DeviceSettingsDialog::deviceListUpdated(QMap<DeviceIndex, DominoSettings> devMap)
{
    devices = devMap;
    QListWidget *list = ui->listWidgetDevices;
    while (list->count()) {
        QListWidgetItem *item = list->takeItem(0);
        delete item;
    }

    foreach (const DeviceIndex& index, devices.keys()) {
        DominoSettings& ds = devices[index];
        if (!ds.enabled) continue;
        new QListWidgetItem(getDevName(index), list);
    }

}

void DeviceSettingsDialog::setMStreamWrite(QString path)
{
    ui->labelMStreamWriteVal->setText(path.isEmpty() ? "disabled" : path);
}

void DeviceSettingsDialog::setMtuSize(int mtu)
{
    ui->labelMtuSize->setText(QString::number(mtu));
}

void DeviceSettingsDialog::setStopConditions(int limit)
{
    ui->labelStopConditionsVal->setText(limit ? QString("EvNum limit(%1)").arg(limit)
                                           : "disabled");
    ui->labelStopConditions->setEnabled(limit);
    ui->labelStopConditionsVal->setEnabled(limit);
}

void DeviceSettingsDialog::on_listWidgetDevices_currentItemChanged(QListWidgetItem *current, QListWidgetItem *)
{
    QPlainTextEdit *devInfo = ui->plainTextEditInfo;
    devInfo->clear();

    if(!current)
        return;

    foreach (const DeviceIndex& index, devices.keys()) {
        DominoSettings& ds = devices[index];
        if (!ds.enabled) continue;
        if(current->text() == getDevName(index)){
            devInfo->setPlainText(ds.toString());
            break;
        }
    }
}

QString DeviceSettingsDialog::getDevName(const DeviceIndex &index) const
{
    return QString("%1 %2")
            .arg(getDeviceTypeName(index.getDevId()))
            .arg(index.getSerialStr());
}
