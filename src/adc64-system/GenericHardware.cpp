//
//    Copyright 2013 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "GenericHardware.h"

#include <QTimer>
#include "mregdev/qtmregdevice.h"

namespace  {
const int statusPollTimeoutMs = 1000; // milliseconds
}

GenericHardware::GenericHardware(QtMregDevice *device) :
    QObject(0), dev(device), connectedState(false),updateStatusTimer(new QTimer(this))
{
    connect(dev, SIGNAL(deviceConnected()), SLOT(on_deviceConnected()));
    connect(dev, SIGNAL(deviceDisconnected()), SLOT(on_deviceDisconnected()));
    connect(updateStatusTimer, SIGNAL(timeout()), SLOT(updateStatus()));
    updateStatusTimer->setSingleShot(true);
}

GenericHardware::~GenericHardware()
{
    updateStatusTimer->stop();
    delete dev;
}

QString GenericHardware::getAddressStr() const
{
    return dev->getAddressStr();
}

QString GenericHardware::getDeviceInfoStr() const
{
    return dev->getDeviceInfoStr();
}

bool GenericHardware::isConnected() const
{
    return connectedState;
    //Not correct cause in moment of detect() it will return true
//    return dev->isConnected();
}

void GenericHardware::on_deviceConnected()
{
    connectedState = true;
    writeSetup();
    updateStatus();
}

void GenericHardware::on_deviceDisconnected()
{
    connectedState = false;
    updateStatusTimer->stop();
}

void GenericHardware::updateStatus()
{
    if(connectedState)
        updateStatusTimer->start(statusPollTimeoutMs);
}
