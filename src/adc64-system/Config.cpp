#include "Config.h"
#include "daq-config/DaqConfig.h"
#include "adc64-common/dominosettings.h"

#include "QDebug"
#include "QJsonDocument"
#include "QStandardPaths"

const QString DEVICE = "device";
const QString DEFAULT = "default";

Config::Config(StartupOptions &startup_options):
    program_type(startup_options.program_type),
    program_index(startup_options.program_index),
    configuration_name(startup_options.configuration_name),
    run(false),
    allow_remote_control(true),
    write_enable(true),
    show_details(false),
    event_number_limit(0),
    mtu_size(1500),
    trig_on_xoff_stop(true)
{
    update_root();
    default_root = root;
    default_root.set_data("configuration_name", DEFAULT);
}

void Config::read_config()
{
    root = BaseConfig::read_config(program_type, program_index, configuration_name);
    if(root.isEmpty()){
        root.set_data("program_type", program_type);
        root.set_data("program_index", program_index);
        root.set_data("configuration_name", configuration_name);
        BaseConfig::write_config(root);
    }

    program_index = root.get_data<QString>("program_index");
    configuration_name = root.get_data<QString>("configuration_name");
    runId.runIndex = root.get_data<QString>("run_index");
    runId.runNumber = root.get_data<int>("run_number");
    run = false;
    run_time_str = root.get_data<QString>("run_time_str");
    allow_remote_control = root.get_data<bool>("allow_remote_control");
    write_enable = root.get_data<bool>("write_enable");
    storage_path = root.get_data<QString>("storage_path");
    show_details = root.get_data<bool>("show_details");
    event_number_limit = root.get_data<quint64>("event_number_limit");
    mtu_size = root.get_data<quint16>("mtu_size", mtu_size);
    if(mtu_size==0)
        mtu_size = 1500;
    trig_on_xoff_stop = root.get_data<bool>("trig_on_xoff_stop");

    device_configs.clear();
    for(QString child_name : root.children().keys()){
        if(child_name.startsWith(DEVICE)){
            RootConfig root_child  = root.child(child_name);
            DominoSettings device_config;
            device_config.root_to_values(root_child);
            DeviceIndex device_index = DeviceIndex(device_config.device_id, device_config.device_serial);
            if(!device_index.isNull())
                device_configs[device_index] = device_config;
        }
    }
}

void Config::write_config()
{
    update_root();
    BaseConfig::write_config(root);
}

QMap<DeviceIndex, bool> Config::synchronize_with_adc64()
{
    QMap<DeviceIndex, bool> to_return;
    for(const DeviceIndex &index : device_configs.keys()){
        to_return[index] = false;
    }

    RootConfig adc64_root = BaseConfig::read_config(DaqConfig::getTypeName(DaqConfigAdc64), program_index, configuration_name);
    QMap<DeviceIndex, DominoSettings> device_configs_adc64;


    if(!adc64_root.isEmpty()){
        for(QString child_name : adc64_root.children().keys()){
            if(child_name.startsWith(DEVICE)){
                RootConfig root_child  = adc64_root.child(child_name);
                DominoSettings device_config;
                device_config.root_to_values(root_child);
                DeviceIndex device_index = DeviceIndex(device_config.device_id, device_config.device_serial);
                if(!device_index.isNull())
                    device_configs_adc64[device_index] = device_config;
            }
        }

        bool device_config_changed = false;
        for(const DeviceIndex &index : device_configs.keys()) {
            bool find = false;
            for(const DeviceIndex &index_adc64 : device_configs_adc64.keys()) {
                if(index == index_adc64){
                    device_configs[index] = device_configs_adc64[index];
                    find = true;
                    device_config_changed = true;
                }
            }
            if(find) {
                to_return[index] = true;
            } else {
                to_return[index] = false;
            }
        }

        if(device_config_changed) {
            write_config();
        }
    }

    setMStreamSetup();
    return to_return;
}

RootConfig Config::update_root()
{
    root.remove_all_data();
    root.set_data("program_type", program_type);
    root.set_data("program_index", program_index);
    root.set_data("configuration_name", configuration_name);
    root.set_data("run_index", runId.runIndex);
    root.set_data("run_number", runId.runNumber);
    root.set_data("run", run);
    root.set_data("run_time_str", run_time_str);
    root.set_data("allow_remote_control", allow_remote_control);
    root.set_data("write_enable", write_enable);
    root.set_data("storage_path", storage_path);
    root.set_data("show_details", show_details);
    root.set_data("event_number_limit", event_number_limit);
    root.set_data("mtu_size", mtu_size);
    root.set_data("trig_on_xoff_stop", trig_on_xoff_stop);

    for(DeviceIndex device_index : device_configs.keys()){
        RootConfig root_child = device_configs[device_index].values_to_root();
        root_child.item_name = DEVICE + "_" + device_index.getSerialStr();
        root.append_child(root_child);
    }

    return root;
}

void Config::setMStreamSetup()
{
    for (DeviceIndex index : device_configs.keys()) {
        DominoSettings &ds = device_configs[index];
        ds.mstreamEnable = true;
        ds.run = false;
    }
}
