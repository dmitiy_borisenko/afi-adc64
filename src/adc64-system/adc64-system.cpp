//
//    ADC64 System Application
//
//    Copyright 2013-2015 AFI Electronics
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <exception>
#include <QDebug>
#include <QApplication>
#include <QSettings>
#include <QString>

#include "MainWindow.h"
#include "base/SyslogMessageSender.h"
#include "base/RunGuard.h"
#include "daq-config/DaqConfig.h"
#include "mongo/StartupOptions.h"
#include "mongo/BaseConfig.h"

int main (int argc, char **argv)
{
    QCoreApplication::setOrganizationName("AFI Electronics");
    QCoreApplication::setOrganizationDomain("local");
    QCoreApplication::setApplicationName("ADC64 System");
    QSettings::setDefaultFormat(QSettings::IniFormat);
    SyslogMessageSender::instance().install();

    QString program_type = DaqConfig::getTypeName(DaqConfigAdc64System);
    #ifdef MONGO_DRIVERS
        mongocxx::instance insta{};
    #endif
    BaseConfig base_config;
    base_config.init(program_type);

    int ret = -1;
    try{
        QApplication a(argc, argv);

        StartupOptions startup_options(program_type);
        startup_options.parse_options();
        QString program_index = startup_options.program_index;

        RunGuard runGuard(QString("%1_%2").arg(a.applicationName(), program_index));
        if(!runGuard.tryToRun()) {
            QString errStr = a.applicationName().append(" is already running; ");
            if(program_index.isEmpty())
                errStr += "in local run mode.";
            else
                errStr += QString("with program index '%1'").arg(program_index);
            qFatal("%s", errStr.toStdString().c_str());
        }

        if(startup_options.list){
            return 0;
        }

        MainWindow w(startup_options);
        QObject::connect(&SyslogMessageSender::instance(), &SyslogMessageSender::newMsg,
                         &w, &MainWindow::addLog);
        w.show();
        ret = a.exec();
    } catch (std::exception &e) {
        qCritical() << "Exception: " << e.what ();
    } catch (...){
        qCritical() << "Unknown exception";
    }

    qInfo() << "Shutdown complete";
    return ret;
}
