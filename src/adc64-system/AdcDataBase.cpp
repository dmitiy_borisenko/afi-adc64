#include "AdcDataBase.h"

#include <QMetaType>
#include "QSqlQuery"
#include "QSqlError"
#include "mregdev/fw_version.h"

//Q_DECLARE_METATYPE(fw_version_t);

AdcDataBase::AdcDataBase(QObject *parent):
    DaqDataBase(parent)
{
}

bool AdcDataBase::saveCfgToDB(Config *config, QMap<DeviceIndex, int> *adcCfgIdMap)
{
    bool ok=true;

    QSqlDatabase db = getConnection();
    if(!db.isOpen()) {
        return false;
    }

    foreach (const DeviceIndex &index, config->device_configs.keys()) {
        if(!ok) break;
        const DominoSettings &ds = config->device_configs[index];
        if(!ds.enabled)
            continue;

        int dspId = -1;
        if(ds.dspParams.en) {
            dspId = saveDspConfig(db, ds.dspParams);
            if(dspId < 0) {
                ok = false;
                break;
            }
        }

        int sparseId = -1;
        if(ds.sparseParams.en) {
            sparseId = saveSparseConfig(db, ds.sparseParams);
            if(sparseId < 0)
                ok = false;
        }

        int ampId = -1;
        const bool hasAmp = ds.amp_board_set == AMP_BOARD_AD5622 || ds.amp_board_set == AMP_BOARD_AD5622v2;
        if(hasAmp) {
            ampId = saveAmpConfig(db, ds.amp_board_set, ds.ad5622op);
            if(ampId < 0)
                ok = false;
        }

        int chId = saveChConfig(db, ds);
        if(chId < 0) {
            ok = false;
        }

        // save main config
        int cfgId = 0;
        cfgId = saveGeneralConfig(db, ds, chId, dspId, sparseId, hasAmp, ampId);
        if(cfgId < 0)
            ok = false;
        else {
            if(adcCfgIdMap)
                adcCfgIdMap->insert(index, cfgId);
        }

        if(!ok)
            break;
    }

    if(ok) {
        qInfo() << "All adc64 config was written to DB";
    } else {
        qWarning() << "Failed to write adc64 config to DB";
    }

    return ok;
}

bool AdcDataBase::saveRunToDB(QString runIndex, uint runNumber, Config *config, const DevFwRevMap &adcFwRev)
{
    bool ok;
    QSqlDatabase db = getConnection();
    if(!db.isOpen()) {
        return false;
    }

    QMap<DeviceIndex, int> adcCfgIdMap;
    ok = saveCfgToDB(config, &adcCfgIdMap);
    if(!ok)
        return ok;
    const QString tableName = "RunDeviceConfig";
    QMap<QString, QVariant> params;

    foreach (const DominoSettings &ds, config->device_configs.values()) {
        if(!ds.enabled)
            continue;
        int cfgId = adcCfgIdMap.value(ds.devIndex, -1);
        if(cfgId < 0) {
            qWarning() << "Has missing adcCfgId for device"<<ds.devIndex.getIdent();
            ok = false;
            break;
        }

        params["RunIndex"] = runIndex;
        params["RunNumber"] = runNumber;
        params["DeviceType"] = ds.devIndex.getDevId();
        params["DeviceSerial"] = ds.devIndex.getSerial();
        params["CfgId"] = cfgId;
        params["firmWareRev"] = adcFwRev[ds.devIndex].revision; // FIXME: use full version

        if(!insertQuery(db, tableName, params)) {
            ok = false;
            break;
        }
    }

    if(!ok) {
        const QString warnMsg = "Failed to save configuration to db";
        qWarning() << warnMsg;
        emit runWarning(runIndex, runNumber, warnMsg);
    }

    return ok;
}


int AdcDataBase::saveDspConfig(QSqlDatabase &db, const DspParams &dspParams) const
{
    const QString tableName = "AdcDspConfig";
    QMap<QString, QVariant> params;
    int firId = -1;
    if(dspParams.fir.en) {
        firId = saveFirConfig(db, dspParams.fir);
        if(firId < 0)
            return -1;
    }
    params["blcThr"] = dspParams.blc_thr;
    params["mafEn"] = dspParams.maf_enable;
    params["mafThr"] = dspParams.maf_enable ? dspParams.maf_tap_sel : QVariant(QVariant::Int);
    params["testEn"] = dspParams.test_enable;
    params["firCfgId"] = dspParams.fir.en ? firId : QVariant(QVariant::Int);

    QVector<int> ids = selectQuery(db, tableName, params);
    if(!ids.isEmpty())
        return ids.first();

    int id;
    if(!insertQuery(db, tableName, params, &id))
        id = -1;
    return id;
}

int AdcDataBase::saveFirConfig(QSqlDatabase &db, const FirParams &fir) const
{
    const QString tableName = "AdcFirConfig";
    QMap<QString, QVariant> params;
    params["roundoff"] = fir.roundoff;
    for(int i=0; i<16;++i)
        params[QString("c_%1").arg(i+1, 2, 10, QChar('0'))] = fir.coef.value(i);

    QVector<int> ids = selectQuery(db, tableName, params);
    if(!ids.isEmpty())
        return ids.first();

    params["presetKey"] = fir.presetKey;
    int id;
    if(!insertQuery(db, tableName, params, &id))
        id = -1;
    return id;
}

int AdcDataBase::saveSparseConfig(QSqlDatabase &db, const SparseParams &sparse) const
{
    const QString tableName = "AdcSparseConfig";
    QMap<QString, QVariant> params;
    params["readCellNum"] = sparse.readCellNum;
    params["offset"] = sparse.offset;
    params["period"] = sparse.period;
    params["pointNumber"] = sparse.pointNumber;

    QVector<int> ids = selectQuery(db, tableName, params);
    if(!ids.isEmpty())
        return ids.first();

    int id;
    if(!insertQuery(db, tableName, params, &id))
        id = -1;
    return id;
}

int AdcDataBase::saveAmpConfig(QSqlDatabase &db, quint16 amp_board_set, const ad5622operation &ad5622op) const
{
    const QString tableName = "AdcAmpConfig";
    QMap<QString, QVariant> params;
    params["boardSet"] = amp_board_set;
    params["baseline"] = ad5622op.data_baseline;
    params["level"] = ad5622op.data_level;

    QVector<int> ids = selectQuery(db, tableName, params);
    if(!ids.isEmpty())
        return ids.first();

    int id;
    if(!insertQuery(db, tableName, params, &id))
        id = -1;
    return id;
}

int AdcDataBase::saveChConfig(QSqlDatabase &db, const DominoSettings &ds) const
{
    const QString tableName = "AdcChConfig";
    QMap<int, QMap<QString, QVariant> > chParams;

    if(ds.nch == 0)
        return -1;

    // search ch config
    QMap<int, QVector<int> > possibleIds;
    bool doSelect = true;
    for(int ch=0; ch<ds.nch; ++ch) {
        QMap<QString, QVariant> &params = chParams[ch];
        params["chNum"] = ch;
        params["en"] = ds.chEn[ch];
        params["thrEn"] = ds.chTrigEn[ch];
        params["trigThr"] = ds.chTrigThr[ch];
        params["zsThr"] = ds.chZsThr[ch];

        if(doSelect) {
            QVector<int> &ids = possibleIds[ch];
            ids = selectQuery(db, tableName, params);
            if(ids.isEmpty())
                doSelect = false;
        }
    }

    int chId = -1;
    foreach (chId, possibleIds[0]) {
        for(int ch=1; ch<ds.nch; ++ch) {
            if(!possibleIds[ch].contains(chId)){
                chId = -1;
                break;
            }
        }
        if(chId!=-1)
            break;
    }

    if(chId != -1) {
        return chId;
    }

    QSqlQuery findMaxIdQuery(db);
    findMaxIdQuery.prepare("SELECT id FROM AdcChConfig ORDER BY id DESC LIMIT 1");
    if (findMaxIdQuery.exec()) {
        if(findMaxIdQuery.next()) {
            bool ok;
            chId = findMaxIdQuery.value(0).toInt(&ok);
            if(ok)
                ++chId;
            else
                chId = -1;
        }
    }
    Q_ASSERT(chId!=-1);

    // save ch config
    for(int ch=0; ch<ds.nch; ++ch) {
        QMap<QString, QVariant> &params = chParams[ch];
        params["id"] = chId;
        if(!insertQuery(db, tableName, params))
            return -1;
    }

    return chId;
}

int AdcDataBase::saveGeneralConfig(QSqlDatabase &db, const DominoSettings &ds,int chId, int dspId, int sparseId, bool hasAmp, int ampId) const
{
    const QString tableName = "AdcConfig";

    // hwStr - doesn't defined by operator, skipp it
    // ds.hwStr; // LLDP-MED[5] Hardware Revision

    // softwareZs - deprecated
    // ds.softwareZs;

    // Don't write reg_pca as AMP_BOARD_PCA deprecated
    // ds.reg_pca;

    // mstreamEnable - always true for adc64-system
    // ds.mstreamEnable;


    QMap<QString, QVariant> params;
    params["nch"] = ds.nch;
    params["ReadoutSize"] = ds.readCellNumber;
    params["ReadoutLatency"] = ds.trigDelay;
    params["TrigTimer"] = ds.trigsetup.bit.timer_en;
    params["TrigThr"] = ds.trigsetup.bit.threshold_en;
    params["TrigLemo"] = ds.trigsetup.bit.ttl_en;
    params["InvertInput"] = ds.invertInput;
    params["InvertThrTrig"] = ds.invert_thr_trig;
    params["ZsEn"] = ds.zs_en;
    params["ZsRising"] = !ds.invert_zs_thr;
    params["ChCfgId"] = chId;
    params["DspCfgId"] = ds.dspParams.en ? dspId : QVariant(QVariant::Int);
    params["SparseCfgId"] = ds.sparseParams.en ? sparseId : QVariant(QVariant::Int);
    params["AmpCfgId"] = hasAmp ? ampId : QVariant(QVariant::Int);
    params["mtu"] = ds.mtuSize;

    QVector<int> ids = selectQuery(db, tableName, params);
    if(!ids.isEmpty())
        return ids.first();

    int id;
    if(!insertQuery(db, tableName, params, &id))
        id = -1;
    return id;
}
