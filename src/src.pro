TEMPLATE = subdirs
SUBDIRS += adc64 adc64-system

unix {
    cppcheck-xml.commands = for i in $$SUBDIRS; do $(MAKE) -C \$$i cppcheck-xml; done
    QMAKE_EXTRA_TARGETS += cppcheck-xml

    cppcheck.commands = for i in $$SUBDIRS; do $(MAKE) -C \$$i cppcheck; done
    QMAKE_EXTRA_TARGETS += cppcheck

    cpplint.commands = for i in $$SUBDIRS; do $(MAKE) -C \$$i cpplint; done
    QMAKE_EXTRA_TARGETS += cpplint
}
